//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
Item itmR = this;
	
try
{
	Item itmT1 = inn.getItemById("In_Timesheet_R2","5E728FEAF837499ABBC2994F7D7C6907");
	//Item Context, string EmailMessageName, string IdentityIds
	_InnH.SendEmail(itmT1,"In_WorkTime_Reject",itmT1.getProperty("in_employee"));
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

	string strError = ex.Message + "\n";
	strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
	{
	strError += "無法執行AML:" + aml  + "\n";;
	}
	if(sql!="")
	{
		strError += "無法執行SQL:" + sql  + "\n";;
	}	
	string strErrorDetail="";
	strErrorDetail = strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	return inn.newError(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_EmailTest' and [Method].is_current='1'">
<config_id>6E9F78DC5EAB407BBB672BC69417203D</config_id>
<name>In_EmailTest</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
