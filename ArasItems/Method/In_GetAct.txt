//System.Diagnostics.Debugger.Break();
//in_GetAct

//Modify by Louis 2015/3/12
//允許傳入 傳入Activity ID(actid) 或者 ActivityAssignment ID(actassid)

Innovator inn  = this.getInnovator();
string criteria = this.getProperty("criteria");
string aml = "";
string actassid="";
string UserInfo = this.getProperty("userinfo","");
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);

string lang = _InnoApp.GetUserInfo(UserInfo, "lang");
//可以傳入Activity ID(actid) 或者 ActivityAssignment ID(actassid)
string identity_list = Aras.Server.Security.Permissions.Current.IdentitiesList;
Item actass;
if(criteria.IndexOf("actassid")>=0)
{
	actassid = criteria.Replace("actassid=","");	
}
if(criteria.IndexOf("actid")>=0)
{
	//傳入Activity ID(actid) 
	//取得這個USER的所有 identity
	
	//identity_list = identity_list.Replace("'","");
	aml = "<AML>";
	aml += "<Item type='Activity Assignment' action='get' select='id'>";
	aml += "<source_id>" + criteria.Replace("actid=","") + "</source_id>";
	aml += "<related_id condition='in'>" + identity_list + "</related_id>";
	aml += "</Item></AML>";
	
	actass = inn.applyAML(aml);
	if(actass.isError())
	{
		throw new Exception(_InnoApp.Getl10nValue("in_ui_resources","in_GetAct.NotAllowVote","無簽審權限"));
	}
	actassid = actass.getItemByIndex(0).getID();
}
if(criteria.IndexOf("itemtype")>=0 && criteria.IndexOf("itemid")>=0)
{
	/*傳入 Itemtype 與 Id
	1.取得 該 Item 的  in_wfp_id 屬性
	2.找到該workflow process 內 Active 的 Activity Assignment, 且 related_id 包含於 目前登入者的 identitylist內	
	*/
	string strItemtype = _InnoApp.GetParameterValue(criteria,"itemtype",':','=');
	string strItemId = _InnoApp.GetParameterValue(criteria,"itemid",':','=');
	
	aml = "<AML>";
	aml += "<Item type='" + strItemtype + "' action='get' select='in_wfp_id' id='" + strItemId + "'>";
	aml += "</Item></AML>";
	
	Item itmWFP = inn.applyAML(aml);
	if(itmWFP.isError())
	{
		throw new Exception(_InnoApp.Getl10nValue("in_ui_resources","in_GetAct.NotAllowVote","無簽審權限"));
	}
	string strWFPId = itmWFP.getProperty("in_wfp_id","");
	
	aml = "<AML>";
	aml += "<Item type='Workflow Process Activity' action='get' select='related_id'>";
	aml += "<source_id>" + strWFPId + "</source_id>";
	aml += "<related_id>";
	aml += "<Item type='Activity' action='get'>";
	aml += "<state>Active</state>";
	aml += "</Item>";
	aml += "</related_id>";
	aml += "</Item></AML>";
	
	Item itmWFPA = inn.applyAML(aml);
	if(itmWFPA.isError())
	{
		throw new Exception(_InnoApp.Getl10nValue("in_ui_resources","in_GetAct.NotAllowVote","無簽審權限"));
	}
	string strActivityIds = "";
	for(int i=0;i<itmWFPA.getItemCount();i++)
	{
		strActivityIds += "'" + itmWFPA.getItemByIndex(i).getProperty("related_id","") + "',";
	}
	strActivityIds = strActivityIds.TrimEnd(',');
	
	aml = "<AML>";
	aml += "<Item type='Activity Assignment' action='get' select='id'>";
	aml += "<source_id condition='in'>" + strActivityIds + "</source_id>";
	aml += "<related_id condition='in'>" + identity_list + "</related_id>";
	aml += "</Item></AML>";
	
	actass = inn.applyAML(aml);
	if(actass.isError())
	{
		throw new Exception(_InnoApp.Getl10nValue("in_ui_resources","in_GetAct.NotAllowVote","無簽審權限"));
	}
	actassid = actass.getItemByIndex(0).getID();
}

string status = "false";
string str_r = "";
//criteria : itemid=xxxxxxxxx



//依據ActID 取得 Activity

aml = "<AML>";
aml += "<Item type='Activity' action='get'>";
aml +="	<Relationships>";
aml +="		<Item type='Activity Assignment' action='get'><id>" + actassid + "</id></Item>";
aml +="		<Item type='Activity Task' action='get' language='" + lang + "'/>";
aml +="		<Item type='Activity Variable' action='get'/>";
aml +="		<Item type='Workflow Process Path' action='get' language='" + lang + "'/>";
aml +="	</Relationships>";
aml +="</Item>";
aml += "</AML>";

Item Act = inn.applyAML(aml);
string ActivityID="";
if(Act.isError())
{
	str_r = Act.getErrorString();
	status = "false";
}
else
{
	str_r =Act.dom.SelectSingleNode("//Result").InnerXml;
	ActivityID = Act.getID();
	status = "true";
}

//取得workflow_name,attachedType(Itemetype),attachedId(Item id),workflowProcessId
//Item-(workflow)-Workflow Process-(workflow Process Activity)-Activity

aml = "<AML>";
aml += "<Item type='Workflow Process Activity' action='get' select='source_id(id,name)'>";
aml += "<related_id>" + ActivityID + "</related_id>";
aml += "</Item>";
aml += "</AML>";

Item q = inn.applyAML(aml);
string workflowProcessId = q.getPropertyItem("source_id").getID();
string workflow_name = q.getPropertyItem("source_id").getProperty("name","");

aml = "<AML>";
aml += "<Item type='Workflow' action='get' select='source_id,source_type(name),related_id,name'>";
aml += "<related_id>" + workflowProcessId + "</related_id>";
aml += "</Item>";
aml += "</AML>";

 q = inn.applyAML(aml);
string attachedType = q.getPropertyItem("source_type").getProperty("name","");
string attachedId = q.getProperty("source_id","");

str_r += "<workflow_name>" + workflow_name + "</workflow_name>";
str_r += "<workflowProcessId>" + workflowProcessId + "</workflowProcessId>";
str_r += "<attachedType>" + attachedType + "</attachedType>";
str_r += "<attachedId>" + attachedId + "</attachedId>";

//補上 InnoControlledItem
Item itmControlledItem = _InnH.GetInnoControlledItem(Act);
string InnControlledItemType = itmControlledItem.getType();
string InnControlledItemId = itmControlledItem.getID();
str_r += "<in_controlled_itemtype>" + InnControlledItemType + "</in_controlled_itemtype>";
str_r += "<in_controlled_itemid>" + InnControlledItemId + "</in_controlled_itemid>";




string r = "";
criteria = "<request><![CDATA[" + criteria + "]]></request>";
try
{

r = _InnoApp.BuildResponse(status,str_r,"",criteria);
	
}
catch (Exception ex)
{
	r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);

}


return inn.newResult(r);
  
  
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetAct' and [Method].is_current='1'">
<config_id>33C050B61C204C928C0D893F8E605668</config_id>
<name>In_GetAct</name>
<comments>Add By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
