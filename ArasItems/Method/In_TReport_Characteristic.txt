Innovator inn = this.getInnovator();
//System.Diagnostics.Debugger.Break();
string Parameter = "";

//先判斷本身的In_Drug TReport Characteristic是否有值,如果已經有Relationship, 則忽略不做
Item itmIn_Drug_In_Drug_Characteristic = inn.newItem("In_Drug TReport In_Drug Char","get");
itmIn_Drug_In_Drug_Characteristic.setProperty("source_id",this.getID());
itmIn_Drug_In_Drug_Characteristic = itmIn_Drug_In_Drug_Characteristic.apply();
if(itmIn_Drug_In_Drug_Characteristic.getItemCount()>0)
	return this;


Item itmIn_Drug_Part = inn.newItem("In_Drug","get");
itmIn_Drug_Part.setProperty("source_id",this.getID());
itmIn_Drug_Part = itmIn_Drug_Part.apply();

//for(int i=0;i<itmIn_Drug_Part.getItemCount();i++)
//{
	var in_relpart_id = this.getProperty("in_rel_In_Drug","");
	if(in_relpart_id!=""){
		Item itm = inn.getItemById("In_Drug",in_relpart_id);
		if(!itm.isError()){
			Parameter="<FromRelName>In_Drug In_Drug Characteristic</FromRelName>";
			Parameter += "<ToRelName>In_Drug TReport In_Drug Char</ToRelName>";
			Parameter += "<FromSourceID>" + itm.getProperty("id","") + "</FromSourceID>";
			Parameter += "<ToSourceID>" + this.getID() + "</ToSourceID>";
			Parameter += "<FromSourceName>In_Drug</FromSourceName>";
			Parameter += "<ToSourceName>In_Drug TestReport</ToSourceName>";
		//	Parameter += "<CopyProperies>in_fda_date,in_fda_standard,in_gmp_standard</CopyProperies>";
			Parameter += "<CopyProperies>related_id,in_target,in_fda_date,in_gmp_date,in_fda_standard,in_gmp_standard,in_gcp,in_glp,in_decision_condition,in_determination_results</CopyProperies>";
			
			Item r = inn.applyMethod("in_CopyRelationships",Parameter);
		}
	}
//}

return this;



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_TReport_Characteristic' and [Method].is_current='1'">
<config_id>AD834957ADAA4B26A79EB980EDD2ADEB</config_id>
<name>In_TReport_Characteristic</name>
<comments>Add by In</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
