/*
目的:因為DLL無法取到Aras.Server.Security.Permissions,所以透過本METHOD回傳identitylist
做法:
參數:<user_id>user的id</user_id>, 若沒傳則以current user 的 id 作為預設
*/
// //System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

string userId = this.getProperty("user_id",this.getProperty("userId",""));
string idenIds = Aras.Server.Security.Permissions.GetIdentitiesList(CCO.DB.InnDatabase, userId);

// return inn.newResult(idenIds);

Item itmR = inn.newItem();
itmR.setProperty("Result",idenIds);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetIdentityList' and [Method].is_current='1'">
<config_id>D9E9244DEA9C43F3B60101DD63333B85</config_id>
<name>In_GetIdentityList</name>
<comments>回傳給DLL identityList</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
