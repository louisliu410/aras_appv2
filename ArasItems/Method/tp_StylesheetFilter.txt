return {
	'source_id': {filterValue: aras.getItemProperty(item, 'id'), isFilterFixed: true}
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_StylesheetFilter' and [Method].is_current='1'">
<config_id>F04872EA4C7341E59ACFBCD9716AD3AA</config_id>
<name>tp_StylesheetFilter</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
