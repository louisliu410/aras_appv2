//本方法以newResult方式回傳值， 1: 方法正常結束 2:無改變，不需要處理 
Innovator inn=this.getInnovator();
string functionTimeID=this.getProperty("functiontime_id","no_data");
string dateStart=this.getProperty("date_start","no_data");
string dateEnd=this.getProperty("date_end","no_data");

if(functionTimeID=="no_data" || dateStart=="no_data" || dateEnd=="no_data"){
    string message="Missing parameter functiontime_id= "+functionTimeID+"  dateStart=  "+dateStart+"  dateEnd = "+dateEnd;    
    return inn.newError(message);
}


Item functionTime=inn.newItem();
functionTime.setAttribute("type","In_Meeting_Functiontime");
functionTime.setAttribute("action","get");
functionTime.setID(functionTimeID);
functionTime=functionTime.apply();


string oDateStart=functionTime.getProperty("in_date_s");
string oDateEnd=functionTime.getProperty("in_date_e");


if(oDateStart==dateStart &&oDateEnd==dateEnd){
    return inn.newResult("2");
}
//System.Diagnostics.Debugger.Break();

functionTime.setProperty("in_date_s",System.DateTime.Parse(dateStart).ToString("yyyy-MM-ddTHH:mm:ss"));
functionTime.setProperty("in_date_e",System.DateTime.Parse(dateEnd).ToString("yyyy-MM-ddTHH:mm:ss"));
functionTime.setAttribute("action","edit");
functionTime=functionTime.apply();


if(!functionTime.isError() ||!functionTime.isEmpty()){
    return inn.newResult("1");
}else{
    
    return inn.newError(functionTime.dom.InnerXml);
}



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Set_MeetingFunctiontime' and [Method].is_current='1'">
<config_id>652AFFCA95594506B843EF262175E980</config_id>
<name>In_Set_MeetingFunctiontime</name>
<comments>設定會議的各選項開關時程，由InnoJSON.ashx呼叫</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
