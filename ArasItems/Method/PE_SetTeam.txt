if ("" === this.getProperty("team_id",""))
{
  var inn = this.getInnovator();
  var team = inn.newItem("Team", "get");
  team.setProperty("name", "Product Team");
  team = team.apply();
  
  if(team.isError())
  {
    return this;
  }

  this.setProperty("team_id", team.getID());
  this.setPropertyAttribute("team_id", "keyed_name", team.getProperty("keyed_name", "")); 
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_SetTeam' and [Method].is_current='1'">
<config_id>15A2717A86834C62A3F1C0E1251BED47</config_id>
<name>PE_SetTeam</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
