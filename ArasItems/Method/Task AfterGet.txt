//System.Diagnostics.Debugger.Break();

int count = this.getItemCount();
if (count < 1) 
{
	return this;
}
	Innovator inn = this.getInnovator();
string[] identityList = Aras.Server.Security.Permissions.Current.IdentitiesList.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries);
var identityHash = new HashSet<string>(identityList.Select(id => id.Trim(new[] { '\'' })));

for (int i = 0; i < count; i++)
{
	Item itm = this.getItemByIndex(i);
	string assignedTo = itm.getProperty("assigned_to", "");
	bool isAssigned = identityHash.Contains(assignedTo);
	if (isAssigned)
	{
		itm.setProperty("classification", "My");
	}
	else
	{
		itm.removeProperty("classification");
	}
// 	itm.setProperty("my_assignment", isAssigned ? "1" : "0");
    itm.setProperty("my_assignment", "1");


	string itemType = String.Empty;
	if (itm.getType() == "InBasket Task")
	{
		string realItemTypeID = itm.getProperty("itemtype");
		if (!String.IsNullOrEmpty(realItemTypeID))
		{
			Item realItemType = inn.getItemById("ItemType", realItemTypeID);
			itemType = realItemType.getProperty("keyed_name");
		}
	}
	else
	{
		itemType = itm.getType();
	}

	if (itemType == "Project Task")
	{
		string taskId = itm.getID();
		string status = itm.getProperty("status");
		Item activity2Assignment = null;

		Item activity2 = inn.getItemById("Activity2", taskId);
		if (activity2 == null)
		{
			activity2Assignment = inn.getItemById("Activity2 Assignment", taskId);
			if (activity2Assignment != null)
			{
				activity2 = inn.getItemById("Activity2", activity2Assignment.getProperty("source_id"));
			}
		}

		if (activity2 != null && !String.IsNullOrEmpty(activity2.getProperty("status")))
		{
			itm.setProperty("css", String.Format("{0}\n.start_date{{background-color:{1};}}\n.due_date{{background-color:{1};}}", itm.getProperty("css"), activity2.getProperty("status")));
		}

		if (activity2Assignment != null)
		{
			string assignmentState = activity2Assignment.getProperty("state");
			if (!string.Equals(assignmentState, status, StringComparison.Ordinal))
			{
				itm.setProperty("status", assignmentState);
			}
		}
	}

	if(itemType == "Workflow Task")
	{
	    //string strProcessID = itm.getProperty("container");
	   //Item itmWFP = inn.getItemById("Workflow Process",strProcessID);
	    //itm.setProperty("name",itmWFP.getProperty("in_current_activity_name",""));
	}
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Task AfterGet' and [Method].is_current='1'">
<config_id>506D5C1DCC1F435DA2F400B639E467CC</config_id>
<name>Task AfterGet</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
