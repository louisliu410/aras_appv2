var topWindow = aras.getMostTopWindowWithAras(window);
var editorContainer = topWindow.techDocContainer;

function disableField(fieldName) {
	var classificationField = getFieldByName(fieldName);
	var fieldId = classificationField.id.replace('span', '');
	var functionName = 'expression_' + fieldId + '_setExpression';
	setTimeout(functionName + '(false)', 1);
	var inputs = classificationField.getElementsByTagName('input');
	for (var i = 0; i < inputs.length; i++) {
		inputs[i].disabled = 'disabled';
	}
}

if (parent.isTdfPopupDialog) {
	disableField('schema');
	return;
}

if (editorContainer && editorContainer.isEditorLoaded) {
	var editorWindow = editorContainer.domNode.contentWindow;

	if (editorWindow && editorWindow.structuredDocument) {
		editorWindow.structuredDocument.Reload(document.item);
	}
}

function fillContainers() {
	topWindow.registerCommandEventHandler(window, afterSaveItem, 'after', 'save');
}

function afterSaveItem() {
	topWindow.dijit.byId('sidebar').getChildren().filter(function(btn) { return btn.id === 'tp_show_editor'; })[0].domNode.style.display = 'block';
}

if (!window.containersFilled) {
	fillContainers();
	window.containersFilled = true;
}

if (document.isEditMode && !aras.isTempEx(document.item)) {
	disableField('classification');
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_BlockOnFormPopulated' and [Method].is_current='1'">
<config_id>C703CE59C3B14D64982B6483BD5FE6E0</config_id>
<name>tp_BlockOnFormPopulated</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
