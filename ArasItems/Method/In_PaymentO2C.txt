/*
目的:計算客戶費用申請單與廠商費用申請單表頭表身的本幣金額(_o)更新到原幣金額(_c)
位置:客戶費用申請單與廠商費用申請單的 add update
邏輯:
1.將表身的本幣金額(_o)更新到原幣金額(_c)
2.將表頭的本幣金額(_o)更新到原幣金額(_c)
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string strRelationshipTypeName = "";
string strUpdate = "";

Item itmR = this;

try
{
    //1.將表身的本幣金額(_o)更新到原幣金額(_c)
	if(this.getType()=="In_Invoice")
	{
	    strRelationshipTypeName = "In_Invoice_detail_2";
	    
		
	    strUpdate = "in_act_debit_c=in_act_debit_o";
		strUpdate += ",in_nonperiodic_c=in_nonperiodic_o";
		strUpdate += ",in_debit_c=in_debit_o";
		strUpdate += ",in_payment_c=in_payment_o";
		strUpdate += ",in_retention_c=in_retention_o";
		strUpdate += ",in_invoice_tax_c=in_invoice_tax_o";
		strUpdate += ",in_invoice_c=in_invoice_o";
		strUpdate += ",in_tax_c=in_tax_o";
		
		sql = "Update [In_Invoice_detail_2] set ";
		sql += strUpdate;
		sql += " where source_id='" + this.getID() + "'";
	}
	else
	{
	    if(this.getProperty("in_contract","")=="")
	        strRelationshipTypeName = "In_Vendor_Payment_Detail"; //沒合約就用這個
	    else
	        strRelationshipTypeName = "In_Vendor_Payment_detail_2"; //有合約就用這個
		strUpdate += "in_payment_c=in_payment_o";
		strUpdate += ",in_debit_c=in_debit_o";
		strUpdate += ",in_invoice_tax_c=in_invoice_tax_o";
		strUpdate += ",in_invoice_c=in_invoice_o";
		strUpdate += ",in_tax_c=in_tax_o";
	    
		
		sql = "Update [" + strRelationshipTypeName +"] set ";
		sql += strUpdate;
		sql += " where source_id='" + this.getID() + "'";
	}
	
	inn.applySQL(sql);
	
	
	//2.將表頭的本幣金額(_o)更新到原幣金額(_c)
	if(this.getType()=="In_Invoice")
	{
		sql = "Update [In_Invoice] set ";
		strUpdate = "in_act_collection_c=in_act_collection_o ";
		strUpdate += ",in_act_debit_c=in_act_debit_o ";
		strUpdate += ",in_debit_c=in_debit_o ";
		strUpdate += ",in_invoice_c=in_invoice_o ";
		strUpdate += ",in_invoice_tax_c=in_invoice_tax_o ";
        strUpdate += ",in_nonperiodic_c=in_nonperiodic_o ";
        strUpdate += ",in_payment_c=in_payment_o ";
        strUpdate += ",in_retention_c=in_retention_o ";
        strUpdate += ",in_tax_c=in_tax_o ";

		sql += strUpdate;
		sql += " where id='" + this.getID() + "'";
	}
	else
	{
	   
	    
		sql = "Update [In_Vendor_Payment] set ";
		strUpdate = "in_act_collection_c=in_act_collection_o ";
		strUpdate += ",in_debit_c=in_debit_o ";
		strUpdate += ",in_invoice_c=in_invoice_o ";
		strUpdate += ",in_invoice_tax_c=in_invoice_tax_o ";
        strUpdate += ",in_payment_c=in_payment_o ";
        strUpdate += ",in_retention_c=in_retention_o ";
        strUpdate += ",in_tax_c=in_tax_o ";
		
		sql += strUpdate;
		sql += " where id='" + this.getID() + "'";
	}
	
	inn.applySQL(sql);
	
	
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_PaymentO2C' and [Method].is_current='1'">
<config_id>0DCA346B0ECE4BF49C8F8A1653D2B2C7</config_id>
<name>In_PaymentO2C</name>
<comments>計算客戶費用申請單與廠商費用申請單表頭表身的本幣金額(_o)更新到原幣金額(_c)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
