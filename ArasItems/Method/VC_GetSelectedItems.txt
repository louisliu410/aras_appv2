var idArray = [];
var isStoreNeed = typeof main !== 'undefined';

if (isStoreNeed) {
	idArray = main.work.gridApplet.getSelectedItemIds(',').split(',');
} else {
	idArray.push(this.getID());
}

//create temporary store
if (isStoreNeed && !main.work['vc_getSelectedItems']) {
	main.work['vc_getSelectedItems'] = [];
}

if (isStoreNeed) {
	main.work['vc_getSelectedItems'].push(this);
}

var self = this;
function handleResultItem(idsArray, selectedItems) {
	// Add a dummy item to start the result collection (removed later)
	var resultItem = self.newItem('DeleteMe', 'DeleteMe');

	for (var i = 0; i < idsArray.length; i++) {
		resultItem.appendItem(selectedItems[i]);
	}
	if (!resultItem.isCollection()) { return; }

	// Remove the dummy item
	resultItem.removeItem(resultItem.getItemByIndex(0));
	return resultItem;
}

if (!isStoreNeed) {
	return handleResultItem(idArray, [this]);
} else if (this.getID() == idArray[idArray.length - 1]) {
	// If this is the last item in the array then build an item collection to return
	var resultItem = handleResultItem(idArray, main.work['vc_getSelectedItems']);
	//delete temporary store
	delete main.work['vc_getSelectedItems'];
	return resultItem;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_GetSelectedItems' and [Method].is_current='1'">
<config_id>DF8802FFCAA847FC99152F0F8F12BF2C</config_id>
<name>VC_GetSelectedItems</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
