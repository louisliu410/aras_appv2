//System.Diagnostics.Debugger.Break();
Innovator inn=this.getInnovator();
//string meetingId=this.getProperty("meeting_id","no_data");
//string agentID=this.getProperty("agent_id","no_data");
string functionTimeType="sheet1";//function time的in_action的值，參數化方便未來修改
bool isFunction=false;
string meetingId=this.getProperty("meeting_id","6908863025B74A008F8A8C1A88E2FBBF");
string agentID=this.getProperty("agent_id","0E53E163F74843AEAABAF65DDC39DCC6");
string surveytype=this.getProperty("survey","1");
string requiredID="";


Item itmQ=inn.newItem("In_Meeting","get");//用來抓未過濾的資料
Item itmR=inn.newItem("In_Meeting","get");//用來乘載要回傳的資料
itmR.setID(meetingId);
itmR=itmR.apply();
itmQ.setID(meetingId);
itmQ=itmQ.apply();
itmR.setProperty("agent_id",agentID);
itmR.fetchRelationships("In_Meeting_Agenda"); 
itmQ.fetchRelationships("In_Meeting_Surveys","*","[In_Meeting_Surveys].sort_order");
itmQ.fetchRelationships("In_Meeting_Functiontime");

//測試狀態中。
//Item functionTime=itmQ.getRelationships("In_Meeting_Functiontime");

Item activeCheck=inn.newItem();
activeCheck.setAttribute("type","Method");
activeCheck.setAttribute("action","In_Get_MeetingIsActive");
activeCheck.setPropertyItem("meeting_item",itmQ);
//因為這邊只要檢查有沒有在報名期間內，所以寫死。
activeCheck.setProperty("function_type","sheet1");
activeCheck=activeCheck.apply();



itmR.setProperty("inn_is_active",activeCheck.getResult());








//檢查會議是否仍在報名期間
/*  sample
DateTime start=System.DateTime.Parse(itmQ.getProperty("in_date_s"));
DateTime end=System.DateTime.Parse(itmQ.getProperty("in_date_e"));
if(start<=System.DateTime.Now && end>=System.DateTime.Now){
    //result.addRelationship(itmQ);
}

*/

//修改日期格式
string date_start=itmR.getProperty("in_date_s");
string date_end=itmR.getProperty("in_date_e");
itmR.setProperty("in_date_s",System.DateTime.Parse(date_start).ToString("yyyy-M-dd  H:mm"));
itmR.setProperty("in_date_e",System.DateTime.Parse(date_end).ToString("yyyy-M-dd  H:mm"));



//過濾掉非指定類型問卷的物件。
//因為現在類別是寫在relationship上，所以要多一到抓值手續
Item surveys= itmQ.getRelationships("In_Meeting_Surveys");
itmQ.createRelationship("In_Meeting_Survey","get");
int surveyCount=surveys.getItemCount();
for(int x=0;x<surveys.getItemCount();x++){
    Item surveyQ=surveys.getItemByIndex(x);
    Item question;
    if(surveyQ.getProperty("in_surveytype","non_exist")==surveytype){
        question=surveyQ.getRelatedItem();
        itmR.addRelationship(question);
        if(question.getProperty("in_request","non_exist")=="1"){
            requiredID+=question.getID()+",";
        }

    }

}
requiredID=requiredID.TrimEnd(new char[]{','});




return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_get_meeting_register' and [Method].is_current='1'">
<config_id>DA36F6182393433AAF22993E8ABA7D74</config_id>
<name>In_get_meeting_register</name>
<comments>本方法提供meeting_register.aspx需要的資訊</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
