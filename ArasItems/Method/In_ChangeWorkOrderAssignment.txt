/*
目的:將派工單目前關卡的人員與簽審者帶到對應關連頁簽(In_WorkOrder_Assignments,In_WorkOrder_Managers)中
做法:
1.取得is_current的派工細項
2.清除所有的簽核人員與簽核主管
3.逐一建立簽核人員與簽核主管
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Item itmWFP = _InnH.GetInnoWorkflowProcessByActivity(this);
string strControlledItem_cfgid = itmWFP.getProperty("in_config_id","");
string strControlledItem_name = itmWFP.getProperty("in_itemtype","");


string strWorkOrderID = strControlledItem_cfgid;


aml = "<AML>";
aml += "<Item type='In_WorkOrder_Detail' action='get' orderBy='sort_order' select='in_new_assignment,sort_order,in_is_current,in_manager'>";
aml += "<source_id>" + strWorkOrderID + "</source_id>";
aml += "<in_is_current>1</in_is_current>";
aml += "</Item></AML>";

Item itmDetails = inn.applyAML(aml);
Item itmDetail;
string strManagers = "";
Item itmTemp;
if(!itmDetails.isError())
{
	//清除所有的簽核人員與簽核主管
	aml = "<AML>";
	aml += "<Item type='In_WorkOrder_Assignments' action='delete' where=\"source_id='" +  strWorkOrderID + "'\">";
	aml += "</Item>";
	aml += "<Item type='In_WorkOrder_Managers' action='delete' where=\"source_id='" +  strWorkOrderID + "'\">";
	aml += "</Item>";
	aml += "</AML>";	
	itmTemp = inn.applyAML(aml);
	
	//逐一建立簽核人員與簽核主管
	for(int k=0;k<itmDetails.getItemCount();k++)
	{
		itmDetail = itmDetails.getItemByIndex(k);
		string strAssignmentID = itmDetail.getProperty("in_new_assignment","");
		string strManagerID = itmDetail.getProperty("in_manager","");
		strManagers += itmDetail.getPropertyAttribute("in_manager","keyed_name") + ",";
		aml = "<AML>";
		if(strAssignmentID=="")
			throw new Exception("查無派工任務[" + itmDetail.getProperty("in_name","") + "]的簽核人員，請重新指派");
		else
		{
			aml += "<Item type='In_WorkOrder_Assignments' action='add'>";
			aml += "<source_id>" + strWorkOrderID + "</source_id>";
			aml += "<related_id>" + strAssignmentID + "</related_id>";
			aml += "</Item>";
		}
		if(strManagerID==""){
			//throw new Exception("查無派工任務[" + itmDetail.getProperty("in_name","") + "]的審查人員，請重新指派");
		}
		else
		{
			aml += "<Item type='In_WorkOrder_Managers' action='add'>";
			aml += "<source_id>" + strWorkOrderID + "</source_id>";
			aml += "<related_id>" + strManagerID + "</related_id>";
			aml += "</Item>";
		}
		aml += "</AML>";
		itmTemp = inn.applyAML(aml);
	}
	
	
	strManagers = strManagers.Trim(',');	
	sql = "Update [In_WorkOrder] set [in_manager]='" + strManagers + "' where id='" + strWorkOrderID + "'";
	inn.applySQL(sql);
	
	
	
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ChangeWorkOrderAssignment' and [Method].is_current='1'">
<config_id>835B26CF672D46A9A4694E6ACFF40878</config_id>
<name>In_ChangeWorkOrderAssignment</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
