if(document.item)
{
  var val = srcElement.value;
  if(val.replace(/\s*\d+\s*/,"").length!=0)
  {
    srcElement.value = "";
    top.aras.AlertError(top.aras.getResource("project", "pr_methods.must_int"));
  }
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='checkInteger' and [Method].is_current='1'">
<config_id>F5384197A8D64920997C8F9E7E0F2C66</config_id>
<name>checkInteger</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
