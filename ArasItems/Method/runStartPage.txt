if (!aras['setup_query'].StartItem) {
	return;
}
var arr = aras['setup_query'].StartItem.split(':');
var type = arr[0];
var id = arr[1];
if (!type || !id) {
	return;
}
var itm = aras.getItemById(type, id, 0);
if (itm) {
	aras.uiShowItemEx(itm, undefined);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='runStartPage' and [Method].is_current='1'">
<config_id>93927AA41EC14ED1839D3AF846C4B8F5</config_id>
<name>runStartPage</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
