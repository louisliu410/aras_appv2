if (propertyName=="is_complete" && newValue=="1")
{
  item.setProperty("percent_compl", "100");
  grid.invalidateContent();
}
else
{
  if (item.getProperty("is_complete")=="1") return false;//the cell is disabled for edit
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_ACW_onCompleteChecked' and [Method].is_current='1'">
<config_id>922E09A792C64ED68B165E6038C8CD2C</config_id>
<name>PM_ACW_onCompleteChecked</name>
<comments>Handles changes of Complete, Percents Complete columns in PM_ACW_Activity2Assmnt grid</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
