/*
目的:檢查特殊字元
做法:
1.取得名稱
2.檢查特殊字元
位置:
onAfterUpdate,onAfterAdd
*/

//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

string strName = "";
string[] arrChar = {"<",">","&","\'","\"","\\"};

if(this.getProperty("name","") != "")
{
    strName = this.getProperty("name","");
}

if(this.getProperty("in_name","") != "")
{
    strName = this.getProperty("in_name","");
}

int intIndex = 0;

try
{
if(strName !="")
{
    for(int i=0;i<arrChar.Length;i++)
    {
        if(strName.Contains(arrChar[i]))
        throw new Exception(_InnH.Translate("主旨或名稱請勿填入特殊字元: < > & \' \\ \" "));
}
}
}
catch(Exception ex)
{
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

    string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();

    strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
    strMethodName = strMethodName.Replace("EventArgs)","");

    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;
    string strError = ex.Message + "\n";

    if(aml!="")
        strError += "無法執行AML:" + aml  + "\n";

    if(sql!="")
        strError += "無法執行SQL:" + sql  + "\n";

    string strErrorDetail="";

    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    Innosoft.InnUtility.AddLog(strErrorDetail,"Error");

    throw new Exception(_InnH.Translate(strError));
}
if(PermissionWasSet)
Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_SpecialCharacters' and [Method].is_current='1'">
<config_id>F9F7914259D24535927F6004F34E408A</config_id>
<name>In_SpecialCharacters</name>
<comments>檢查特殊字元</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
