//傳入參數：
//  meeting_id:會議的id
//  surveytype:問卷種類
//  muid:會議使用者id
//  如果請求的問卷在有效期間內，回傳1
//  如果請求的問卷不再有效期間內，回傳2
//  如果請求的問卷使用者已填寫過，回傳3
string sheetPrefix="sheet";
Innovator inn=this.getInnovator();

string meeting_id=this.getProperty("meeting_id","no_data");
string surveytype=this.getProperty("surveytype","no_data");
string muid=this.getProperty("muid","no_data");
//string meeting_id=this.getProperty("meeting_id","");
//string surveytype=this.getProperty("surveytype","");
//string muid=this.getProperty("muid","");

if(meeting_id=="no_data"||surveytype=="no_data"||muid=="no_data"){
    string errMsg="Missing Parameter meeting_id="+meeting_id+"surveytype= "+surveytype+"  muid=  "+muid;
    return inn.newError(errMsg);
}

DateTime start;
DateTime end;
Item result=inn.newItem();
//檢查使用者是否已填答
Item answers=inn.newItem();
answers.setAttribute("type","In_Meeting_Surveys_result");
answers.setAttribute("action","get");
answers.setProperty("source_id",meeting_id);
answers.setProperty("in_surveytype",surveytype);
answers.setProperty("in_participant",muid);
answers=answers.apply();
//System.Diagnostics.Debugger.Break();


//若使用者以填寫過問卷，傳回3並結束處理。

Item functionTime=inn.newItem();
functionTime.setAttribute("type","In_Meeting_Functiontime");
functionTime.setAttribute("action","get");
functionTime.setProperty("source_id",meeting_id);
functionTime.setProperty("in_action",sheetPrefix+surveytype);
functionTime=functionTime.apply();





//如果找不到對應的In_Meeting_Functiontime時回傳4，基本上不會發生，因為在取得的過程中已經檢查過。但還是擺著以策安全。
if (functionTime.isEmpty()) {
    result.setProperty("active_status","4");
    return result;
    }



start=System.DateTime.Parse(functionTime.getProperty("in_date_s"));
end=System.DateTime.Parse(functionTime.getProperty("in_date_e"));
//System.Diagnostics.Debugger.Break();
result.setProperty("inn_sdate_s",start.ToString("yyyy/MM/dd HH:mm"));
result.setProperty("inn_sdate_e",end.ToString("yyyy/MM/dd HH:mm"));

//如果使用者以填寫過該問卷(答案不為空)，傳回3
if(!answers.isEmpty()){
    result.setProperty("active_status","3");
    return result;
}

//如果在期間內，就傳回1，不在傳回2
if(System.DateTime.Now>=start && System.DateTime.Now<=end){
    result.setProperty("active_status","1");
}else{
    result.setProperty("active_status","2");
}


    return result;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Check_MeetingSurveyActiveObj' and [Method].is_current='1'">
<config_id>09451AFCF52E4963A56B00111E92E59C</config_id>
<name>In_Check_MeetingSurveyActiveObj</name>
<comments>檢查要求的survey是否在有效前間，及是否已填答。內部使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
