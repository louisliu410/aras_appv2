//System.Diagnostics.Debugger.Break();
//in_AddDrug_Approval_Records
//負責將一筆申請記錄複製到多個藥品申請單之下

Innovator inn = this.getInnovator();
string Drug_Approvals = this.getProperty("In_Drug_Lics");
string FromDrug_Approval_RecordsID = this.getProperty("FromDrug_Approval_RecordsID");
string aml = "";
string CloneProps = "in_noticedate,in_note,in_ori_rec_id";


Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

aml = "<AML>";
//aml += "<Item type='In_Drug_Lic_Records' id='" + FromDrug_Approval_RecordsID + "' action='get' select='" + CloneProps + "' />";
aml += "<Item type='In_Drug_Lic_Records' id='" + FromDrug_Approval_RecordsID + "' action='edit' select='" + CloneProps + "'>";
aml += "<in_ori_rec_id>" + FromDrug_Approval_RecordsID + "</in_ori_rec_id>";
aml += "<in_is_ori_rec>1</in_is_ori_rec>";
aml += "</Item>";
aml += "</AML>";

Item FromDrug_Approval_Records = inn.applyAML(aml);
if(FromDrug_Approval_Records.isError())
	throw new Exception(FromDrug_Approval_Records.getErrorDetail());


string[] ClonePropArr = CloneProps.Split(',');
string[] Drug_ApprovalArr = Drug_Approvals.Split(',');
aml = "<AML>";
for(int i=0;i<Drug_ApprovalArr.Length;i++)
{
	aml += "<Item type='In_Drug_Lic_Records' action='add'>";
	aml += "<source_id>" + Drug_ApprovalArr[i] + "</source_id>";
	for(int j=0;j<ClonePropArr.Length;j++)
	{
		aml += "<" + ClonePropArr[j] + ">";
		aml += FromDrug_Approval_Records.getProperty(ClonePropArr[j]);
		aml += "</" + ClonePropArr[j] + ">";
	}
	//aml += "<in_ori_rec_id>" + FromDrug_Approval_RecordsID + "</in_ori_rec_id>";
	aml += "</Item>";
}
aml += "</AML>";

Item itm = inn.applyAML(aml);
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
if(itm.isError())
	throw new Exception(itm.getErrorDetail());
return itm;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_AddDrug_Lic_Records' and [Method].is_current='1'">
<config_id>47F788D009084268B01316B95053C500</config_id>
<name>In_AddDrug_Lic_Records</name>
<comments>Inn drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
