//System.Diagnostics.Debugger.Break();
/*
目的:修正In_DeleteWFF Server Events,補上 onAfterDelete與BefoerDelete
作法:
1.移除所有的 In_DeleteWFF Server Events
2.補上 OnAfter
3.清除殘餘的流程表單
用法:可重複執行
*/

Innovator inn = this.getInnovator();
Item ItemType = null;
Item itmGridEvent = null;
Item itmClientEvent = null;
Item itmServerEvent = null;



string param = "";
string aml = "";
string sql = "";
string strResult = "";

CCO.Utilities.WriteDebug("In_DeleteWFF", "刪除 In_DeleteWFF_Before,In_DeleteWFF 的 Server Events");
Item itmDeleteWFF = inn.getItemByKeyedName("Method","In_DeleteWFF");
string strDeleteWFFId = itmDeleteWFF.getID();


Item itmDeleteWFFBefore = inn.getItemByKeyedName("Method","In_DeleteWFF_Before");
if(itmDeleteWFFBefore.isError())
	throw new Exception("查無[In_DeleteWFF_Before] Method, 請先匯入 In_DeleteWFF_Before Method");
string itmDeleteWFFBefore_Id = itmDeleteWFFBefore.getID();


aml = "<AML>";
aml += "	<Item type='Server Event' action='delete' where=\"related_id='" + strDeleteWFFId + "'\">";
aml += "	</Item>			";
aml += "</AML>";
itmServerEvent = inn.applyAML(aml);

aml = "<AML>";
aml += "	<Item type='Server Event' action='delete' where=\"related_id='" + itmDeleteWFFBefore_Id + "'\">";
aml += "	</Item>			";
aml += "</AML>";
itmServerEvent = inn.applyAML(aml);


aml = "<AML>";
aml += "<Item type='Allowed Workflow' action='get' select='source_id(name,label)' >";
aml += "<source_id condition='ne'>FAC120F9746749EA996D1C84E2C05BF7</source_id>";
aml += "</Item></AML>";
Item itmWorkflowItems = inn.applyAML(aml);
List<string> IDLists = new List<string>();
	
CCO.Utilities.WriteDebug("In_DeleteWFF", "補上 In_DeleteWFF_Before,In_DeleteWFF 的 Server Events");
for(int i=0;i<itmWorkflowItems.getItemCount();i++)
{
	Item itmSourceItem  =itmWorkflowItems.getItemByIndex(i).getPropertyItem("source_id");
	if(IDLists.Contains(itmSourceItem.getID()))
		continue;
	IDLists.Add(itmSourceItem.getID());

	
	aml = "<AML>";
	aml += "	<Item type='Server Event' action='add'>";
	aml += "		<related_id><Item type='Method' action='get' select='id'><name>In_DeleteWFF</name></Item></related_id>";
	aml += "		<server_event>onAfterDelete</server_event>";
	aml += "		<source_id>@ItemId</source_id>";
	aml += "	</Item>			";

	aml += "	<Item type='Server Event' action='add'>";
	aml += "		<related_id><Item type='Method' action='get' select='id'><name>In_DeleteWFF_Before</name></Item></related_id>";
	aml += "		<server_event>onBeforeDelete</server_event>";
	aml += "		<source_id>@ItemId</source_id>";
	aml += "	</Item>			";
		

	aml += "</AML>";

	aml = aml.Replace("@ItemId",itmSourceItem.getID());
	Item itmItemtype = inn.applyAML(aml);

	strResult += itmSourceItem.getProperty("name") + ",";
	CCO.Utilities.WriteDebug("In_DeleteWFF", (i+1).ToString() + "/" + itmWorkflowItems.getItemCount().ToString() + ":" + itmSourceItem.getProperty("keyed_name",""));
	
}

//3.清除殘餘的流程表單(In_WorkflowForm存在,但是真正的簽核物件已不存在)
CCO.Utilities.WriteDebug("In_DeleteWFF", "清除殘餘的流程表單");
sql = "select id,in_itemtype,in_config_id,in_keyed_name from In_WorkflowForm";
Item itmWFFs = inn.applySQL(sql);
for(int i=0;i<itmWFFs.getItemCount();i++)
{
	Item itmWFF =itmWFFs.getItemByIndex(i);
	string strConfigId = itmWFF.getProperty("in_config_id","");
	if(strConfigId=="")
		continue;
	sql = "select id from [" + itmWFF.getProperty("in_itemtype","").Replace(" ","_") + "] where config_id='" + strConfigId + "'";
	Item itmControlledItem = inn.applySQL(sql);
	if(itmControlledItem.getResult()=="")
	{
		//代表找不到簽核物件,流程表單算是孤兒,所以要刪除
		CCO.Utilities.WriteDebug("In_DeleteWFF", "[刪]:" + (i+1).ToString() + "/" + itmWFFs.getItemCount().ToString() + ":" + itmWFF.getProperty("in_itemtype","") + "-" +  itmWFF.getProperty("in_keyed_name",""));
		itmWFF = inn.getItemById("In_WorkflowForm",itmWFF.getProperty("id"));
		itmWFF.apply("delete");
		
	}
	else
	{
		CCO.Utilities.WriteDebug("In_DeleteWFF", "[不刪]:" + (i+1).ToString() + "/" + itmWFFs.getItemCount().ToString() + ":" + itmWFF.getProperty("in_itemtype","") + "-" + itmWFF.getProperty("in_keyed_name",""));
	}
	
}




return inn.newResult(strResult);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DeleteWF_Fix' and [Method].is_current='1'">
<config_id>36B20E294A2043F3A11EACB39477DE78</config_id>
<name>In_DeleteWF_Fix</name>
<comments>修正Innosoft Workflow的Delete機制</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
