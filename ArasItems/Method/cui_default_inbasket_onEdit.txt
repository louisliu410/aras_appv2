var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onEditAttachedCommand) {
	workerFrame.onEditAttachedCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_inbasket_onEdit' and [Method].is_current='1'">
<config_id>22C9E04356C1441E83C84CEE19E19A5A</config_id>
<name>cui_default_inbasket_onEdit</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
