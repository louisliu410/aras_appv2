//System.Diagnostics.Debugger.Break();

Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";

Item itmR = this;

try
{
    if(this.getProperty("in_project_template","")=="")
    {
        throw new Exception(_InnH.Translate("無專案範本,無法轉換專案"));
    }

    aml  = "<AML>";
    aml += "<Item type='Project' action='get' select='wbs_id,name'>";
    aml += "<in_number>"+this.getProperty("item_number")+"</in_number>";
    aml += "</Item></AML>";
    Item itmNewProj = inn.applyAML(aml);

    if(!itmNewProj.isError())
    {
        throw new Exception(_InnH.Translate("專案[" + this.getProperty("item_number")  + "]已存在,無法再次轉換"));
    }

    aml  = "<AML>";
	aml += "<Item type='Project Template' action='get' id='"+this.getProperty("in_project_template","")+"' select='wbs_id,name'/>";
	aml += "</AML>";
	Item itmProjTemplate = inn.applyAML(aml);

	itmProjTemplate.setAttribute("projectCloneMode","CreateProjectFromTemplate");
	itmProjTemplate.setAttribute("action","Project_CloneProjectOrTemplate");
	itmNewProj = itmProjTemplate.apply();

	itmNewProj.setProperty("date_start_target",this.getProperty("in_date_s",""));
	itmNewProj.setProperty("date_due_target",this.getProperty("in_date_e",""));
	itmNewProj.setProperty("in_customer",this.getProperty("in_customer",""));
	itmNewProj.setProperty("name",this.getProperty("name",""));
	itmNewProj.setProperty("in_from_item",this.getProperty("id",""));
	itmNewProj.setProperty("in_number",this.getProperty("item_number",""));
	itmNewProj.setProperty("owned_by_id",this.getProperty("in_pm",""));
	itmNewProj.setProperty("in_company",this.getProperty("in_company",""));
	itmNewProj.setProperty("in_proposal_type",this.getProperty("in_proposal_type",""));
	itmNewProj.setProperty("in_project_type",this.getProperty("in_project_type",""));
	itmNewProj.setProperty("in_dept",this.getProperty("in_dept",""));
	itmNewProj.setProperty("in_temp_budget",this.getProperty("in_temp_budget",""));

	itmNewProj = itmNewProj.apply();
	itmR = itmNewProj;
}
catch(Exception ex)
{
    if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

    string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();

	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");

	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;

	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";

	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");

	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ItemToProject_S' and [Method].is_current='1'">
<config_id>B4D7E25BFDF54A5BB8E1D9A3D86CFFFB</config_id>
<name>In_ItemToProject_S</name>
<comments>單據轉專案</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
