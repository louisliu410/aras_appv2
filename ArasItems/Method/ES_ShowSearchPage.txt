//Get search text
var searchText = '';
var topWindow = aras.getMostTopWindowWithAras(window);

var toolbarApplet = topWindow.menu.activeToolbar;
if (!isNullOrUndefined(toolbarApplet)) {
	var textbox = toolbarApplet.getItem('com.aras.innovator.mwt_es_edit_textbox');
	if (!isNullOrUndefined(textbox)) {
		//Get search text
		searchText = textbox.getText();
	}
}

//Select "Search" in TOC
var treeApplet = topWindow.tree.mainTreeApplet;
if (!isNullOrUndefined(treeApplet)) {
	if (treeApplet.isItemExists('ES_Search')) {
		treeApplet.selectItem('ES_Search');
	} else {
		treeApplet.deselect();
	}
}

//Show search page
var scriptsUrl = aras.getScriptsURL();
topWindow.work.location = '{0}../Modules/aras.innovator.ES/Views/Main.html?q={1}'
	.replace('{0}', scriptsUrl)
	.replace('{1}', encodeURIComponent(searchText));

/*----------------------------------------------------------------------------*/

function isNullOrUndefined(obj) {
	return ((typeof(obj) === 'undefined') || (obj === null));
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='ES_ShowSearchPage' and [Method].is_current='1'">
<config_id>C557750C219F458EAD2B6EE670DFA8CA</config_id>
<name>ES_ShowSearchPage</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
