/*
目的:接收ERP回傳的客戶請款單,廠商請款單,員工費用申請單
參數:
<itemtypename>
<value_o>
<item_number>
<state>
做法:
1.避免進板使用SQL語法累加各單據的erp_sum欄位
2.將結果累加回合約主檔
*/
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;

try
{



	string strAML = this.dom.SelectSingleNode("//AML").OuterXml;
	Item itmPayments = inn.newItem();
	itmPayments.loadAML(strAML);
	for(int i=0;i<itmPayments.getItemCount();i++)
	{
	    Item itmPayment= itmPayments.getItemByIndex(i);
	    string strItemtypeName = itmPayment.getProperty("itemtypename","");
    	string strValue_o = itmPayment.getProperty("value_o","0");
    	string strItemNumber = itmPayment.getProperty("item_number","");


    	aml = "<AML>";
    	aml += "<Item type='" + strItemtypeName + "' action='get'>";
    	aml += "<item_number>" + strItemNumber + "</item_number>";
    	aml += "</Item></AML>";
    	itmPayment = inn.applyAML(aml);
    	if(itmPayment.isError())
    	    throw new Exception(itmPayment.getErrorString());
    	string strState = itmPayment.getProperty("state","");
    	if(strState=="Obsolete")
    	{
    	     if(itmPayment.getProperty("state","")=="Obsolete")
    	        throw new Exception("該單據[" + strItemtypeName + "]:[" + strItemNumber +"]狀態已作廢，無法重複作廢");

    	    if(itmPayment.getProperty("state","")!="Released")
    	        throw new Exception("該單據[" + strItemtypeName + "]:[" + strItemNumber +"]狀態非已發行狀態，無法作廢");
    	     itmPayment.setProperty("state", "Obsolete");
             itmPayment = itmPayment.apply("promoteItem");
    	}
    	else
    	{
        	sql = "Update [" + strItemtypeName.Replace(" ","_") + "] set ";
        	sql += "in_act_collection_o=in_act_collection_o+(" + strValue_o + ")";
        	sql += " where item_number='" + strItemNumber + "' and is_current=1";
        	inn.applySQL(sql);
        	itmR = itmPayment.apply("In_UpdateContractMomey");	    
    	}

	}





}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ToPLMPayment' and [Method].is_current='1'">
<config_id>3BA65C08A954463E901E58D103F4748D</config_id>
<name>In_ToPLMPayment</name>
<comments>接收ERP回傳的客戶請款單,廠商請款單,員工費用申請單</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
