var topWindow = aras.getMostTopWindowWithAras(window);

var deleteSearch = function() {
	var relTabbar = topWindow.relationships ? topWindow.relationships.relTabbar : null;
	var selectedTabId = relTabbar ? relTabbar.GetSelectedTab() : null;
	var selectedTabWnd = selectedTabId ? relTabbar._getTab(selectedTabId).domNode.lastChild.contentWindow : null;

	if (selectedTabWnd && selectedTabWnd.searchContainer) {
		selectedTabWnd.searchContainer._deleteSearch();
	}
};

if (topWindow.tearOffMenuController) {
	topWindow.tearOffMenuController.safeInvokeCommand(deleteSearch);
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_twmm_deleteSearch' and [Method].is_current='1'">
<config_id>44E077A94A6C48D3ADE487D466453BB0</config_id>
<name>cui_default_twmm_deleteSearch</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
