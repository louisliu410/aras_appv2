/*
目的:派工單的工時回報-打卡結束功能, 自動找到正確的工時卡,並填入目前的結束時間
做法:
1.取得 criteria 資料
2.依據criteria取得 in_TimeRecord
3.將in_date_to填入現在時間
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string criteria = this.getProperty("criteria");
string UserInfo = this.getProperty("userinfo","");
string r = "";
string aml = "";
Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

try
{
	string strTimeRecordId = _InnoApp.GetParameterValue(criteria,"timerecord_id",'&',':');
    Item itmTimeRecord = inn.applyMethod("In_TimeRecord_End_S","<timerecord_id>" + strTimeRecordId+ "</timerecord_id>");
	
    criteria = "<request><![CDATA[" + criteria + "]]></request>";
    r = _InnoApp.BuildResponse("true",itmTimeRecord.getID(),"",criteria);
}
catch (Exception ex)
{
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	criteria = "<request><![CDATA[" + criteria + "]]></request>";
    r = _InnoApp.BuildResponse("false",ex.Message,"",criteria);
}

if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
return  inn.newResult(r);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_TimeRecord_End' and [Method].is_current='1'">
<config_id>96BDBFCCD1A54230A8E646526A8243DE</config_id>
<name>In_TimeRecord_End</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
