if (inArgs.isReinit) {
	//Workflow Map, Form, Life Cycle Map, CMF Admin Panel Toolbar
	var node;
	if (window.itemTypeName === 'Form' || window.itemTypeName === 'cmf_ContentType' || window.itemTypeName === 'Life Cycle Map') {
		node = window.item;
	} else {
		node = window.currWFNode;
	}

	if (node) {
		var isTemp = aras.isTempEx(node);
		return {'cui_disabled': !(!isEditMode || isTemp)};
	}
	//Normal Toolbar
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calc_tearoff_states');
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}
	return {'cui_disabled': !inArgs.eventState.isPurge};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_twt_delete' and [Method].is_current='1'">
<config_id>48A053F85D4C48C88AA8044D966589C2</config_id>
<name>cui_reinit_twt_delete</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
