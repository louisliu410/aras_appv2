/*
目的:依據優先度判斷是否放行
做法:
1.取得派工單
2.取得延期次數
3.優先度判斷(選項1,2)
4.優先度判斷(選項3)
5.優先度判斷(選項4)
6.更新欄位
位置:
onActivate
*/

//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string sql = "";

//1.取得派工單
Item ControlledItem = _InnH.GetInnoControlledItem(this);

//2.取得延期次數
int intExtendCount = Convert.ToInt32(ControlledItem.getProperty("in_extend_count","0"));

//3.優先度判斷(選項1,2)
if(ControlledItem.getProperty("in_priority","") == "1" || ControlledItem.getProperty("in_priority","") == "2"){
	return inn.newError(_InnH.Translate("此派工單不得延遲"));
}

//4.優先度判斷(選項3)
if(ControlledItem.getProperty("in_priority","") == "3"){
	if(intExtendCount >= 1){
		return inn.newError(_InnH.Translate("此派工單延遲最多1次，延期不得超過3天"));
	}
}

//5.優先度判斷(選項4)
if(ControlledItem.getProperty("in_priority","") == "4"){
	if(intExtendCount >= 2){
		return inn.newError(_InnH.Translate("此派工單延遲最多2次，延期不得超過7天"));
	}
}

intExtendCount ++;

//6.更新欄位
sql = "Update [In_WorkOrder] set ";
sql += "in_extend_count = '" + intExtendCount.ToString() + "'";
sql += " where id = '" + ControlledItem.getID() + "'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Priority_Judge' and [Method].is_current='1'">
<config_id>8DB076A034304EF1BEFFBF91CCE331ED</config_id>
<name>In_Priority_Judge</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
