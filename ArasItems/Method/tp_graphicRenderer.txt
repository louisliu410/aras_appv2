return {
	constructor: function(args) {},

	RenderInnerContent: function(/*WrappedObject*/renderObject) {
		return this.inherited(arguments);
	}
};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_graphicRenderer' and [Method].is_current='1'">
<config_id>E05B4ABD55484EDD84F6DAA4608E0B54</config_id>
<name>tp_graphicRenderer</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
