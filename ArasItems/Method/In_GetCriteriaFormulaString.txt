'//System.Diagnostics.Debugger.Break() 

Dim inn As Innovator = Me.getInnovator()
Dim err As Item

If Me.GetType() <> "Criteria" Then
	Return Me
End If

If Me.getProperty("description") <> "" Then
	Return Me
End If

Dim cfs As Item = inn.newItem("Criteria Formula", "get")
cfs.setProperty("source_id", Me.getID())
Dim r_cfs As Item = cfs.apply()

If r_cfs.getItemCount() < 0 Then
	err = inn.newError(r_cfs.getErrorDetail())
	Return err
End If

Dim count As Integer = r_cfs.getItemCount()
Dim i As Integer
Dim cf_str As String = ""
Dim cf As Item

For i = 0 To count -1 
	cf = r_cfs.getItemByIndex(i)
	
'	cf_str = cf_str + " " + cf.getProperty("left_parenthesis") + " " + cf.getProperty("property") + " " + cf.getProperty("operator") +" " + cf.getProperty("value") +" " + _
'	                                cf.getProperty("right_parenthesis") + " " + cf.getProperty("and_or")

	cf_str = cf_str + " " + cf.getProperty("left_parenthesis") + " " + cf.getPropertyAttribute("property", "keyed_name") + " " + cf.getProperty("operator") +" " + cf.getProperty("value") +" " + _
	                                cf.getProperty("right_parenthesis") + " " + cf.getProperty("and_or")
Next 

Dim sql_str As String

If cf_str <> "" Then
	Me.setProperty("description", cf_str.substring(0, iif(cf_str.Length > 4000, 4000, cf_str.Length)))	
	
	cf_str = cf_str.Replace("'", "''")
	sql_str = "Update Criteria Set description = '" + cf_str + "' Where id = '" + Me.getID() + "'"
	Dim r As Item = inn.applySQL(sql_str)
	
	If r.isError() Then
		err = inn.newError(r.getErrorDetail())
		Return err
	End If
End If

Return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetCriteriaFormulaString' and [Method].is_current='1'">
<config_id>1ECD08D6B1F341EFB69A95CA76500DE4</config_id>
<name>In_GetCriteriaFormulaString</name>
<comments>Add by IN</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
