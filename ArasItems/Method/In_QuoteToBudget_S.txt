/*
目的:產生一個對應的預算表
做法:
1.建立一個預算表
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";
	Item itmNewBudget =null;
try
{	
	itmNewBudget = inn.newItem("In_Budget","get");
	itmNewBudget.setProperty("in_refquote",this.getID());	
	itmNewBudget.setAttribute("orderBy","modified_on DESC");	
	itmNewBudget.setAttribute("maxRecords","1");
	itmNewBudget = itmNewBudget.apply();
	
	
	
		
	string strBudgetCount = "";
	if(itmNewBudget.isError())
	{
		Item itmBasedBudget = inn.newItem("In_Budget","get");
		itmBasedBudget.setProperty("item_number","SYS-001");
		itmBasedBudget.setAttribute("levels","1");
		itmBasedBudget = itmBasedBudget.apply();
	
		itmNewBudget = itmBasedBudget.clone(true);
		itmNewBudget.setProperty("in_refquote",this.getID());
		itmNewBudget.setProperty("in_name",this.getProperty("in_name",""));
		itmNewBudget.setProperty("in_budget_limit",this.getProperty("in_budget_limit","0"));
		itmNewBudget.setProperty("owned_by_id",this.getProperty("owned_by_id",""));
		itmNewBudget = itmNewBudget.apply();
	}
	else
	{
		throw new Exception("預算表已存在");
	}
	
	
	
		
	

	
	
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmNewBudget;


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_QuoteToBudget_S' and [Method].is_current='1'">
<config_id>3AB01E259A1A448795C933567D9B68AC</config_id>
<name>In_QuoteToBudget_S</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
