' name: restrictAffectedTypeOnServer
' purpose: PLM. Checks in onBeforeAdd and onBeforeUpdate server events type of affected_id and new_item_id properties.
' created: by Valentine Shapovalov
' MethodTemplateName=VBMain;

Dim innovator As Aras.IOM.Innovator = Me.getInnovator()
Dim thisErr As Aras.IOM.Item
Dim relatedItem As Aras.IOM.Item

Function Main() As Item
    relatedItem = Me.getRelatedItem()

    If relatedItem Is Nothing Then
        Dim qry As Aras.IOM.Item = Me.newItem("Affected Item", "get")
        qry.setAttribute("select", "affected_id,new_item_id")
        qry.setID(Me.getRelatedItemID())

        relatedItem = qry.apply()

        If relatedItem.isError() Then
            Return relatedItem
        End If
    End If

    Dim itemTypeName As String = Me.GetType()

    If Not String.Equals(itemTypeName, "Express ECO Affected Item", StringComparison.Ordinal) _
        AndAlso Not String.Equals(itemTypeName, "Express DCO Affected Item", StringComparison.Ordinal) _
    Then
        'We don't want to support different relationships yet
        Return Nothing
    End If

    If Not CheckPropertyValue("affected_id", thisErr) OrElse Not CheckPropertyValue("new_item_id", thisErr) Then
        Return thisErr
    End If

    Return Nothing
End Function

Function GetTargetType(ByVal id As String) As String
    Dim qry As Aras.IOM.Item = Me.newItem("Change Controlled Item", "get")
    qry.setAttribute("select", "id")
    qry.setID(id)

    Dim res As Aras.IOM.Item = qry.apply()

    If res.isError() Then
        Return Nothing
    End If

    Return res.getAttribute("type")
End Function

Function CheckPropertyValue(ByVal prop_name As String, ByRef thisErr As Item) As Boolean
    If String.IsNullOrEmpty(prop_name) Then
        thisErr = innovator.newError("Property name couldn't be empty")
        Return False
    End If

    Dim propertyValue As String = relatedItem.getProperty(prop_name, "")

    If String.IsNullOrEmpty(propertyValue) Then
        ' If property not specified, skip checking without error.
        Return True
    End If

    Dim propertyValueType As String = GetTargetType(propertyValue)

    If String.IsNullOrEmpty(propertyValueType) Then
        thisErr = innovator.newError(String.Format(CultureInfo.InvariantCulture, "Can not get type for value of property '{0}' ", prop_name))
        Return False
    End If

    Dim itemTypeName As String = Me.GetType()

    'Approved list:
    ' ECO <-> Part
    ' DCO <-> Document
    If String.Equals(itemTypeName, "Express ECO Affected Item", StringComparison.Ordinal) _
        AndAlso Not String.Equals(propertyValueType, "Part", StringComparison.Ordinal) _
      OrElse _
      String.Equals(itemTypeName, "Express DCO Affected Item", StringComparison.Ordinal) _
      AndAlso _
      (Not String.Equals(propertyValueType, "Document", StringComparison.Ordinal) _
       AndAlso _
      Not String.Equals(propertyValueType, "CAD", StringComparison.Ordinal)) _
    Then
        thisErr = innovator.newError(String.Format(CultureInfo.InvariantCulture, "Invalid item was selected for current relationship type: RelationshipType '{0}' couldn't contain '{1}' item", itemTypeName, propertyValueType))
        Return False
    End If

    Return True
End Function
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='restrictAffectedTypeOnServer' and [Method].is_current='1'">
<config_id>BFC4BCC651C545B79FF2CD6AD56AC649</config_id>
<name>restrictAffectedTypeOnServer</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
