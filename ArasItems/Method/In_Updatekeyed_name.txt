/*
目的:重組keyed_name
做法:
*/
string strMethodName = "In_Updatekeyed_name";
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string strName = "";

Item itemR = this;

try
{
	
	switch(this.getType())
	{
		case "In_WorkOrder_Detail":
			strName = this.getProperty("in_name","");

			aml = "<AML>";
			aml += "<Item type='In_WorkOrder' action='get'>";
			aml += "<id>"+this.getProperty("source_id","")+"</id>";
			aml += "</Item></AML>";
			Item itmWorkOrder = inn.applyAML(aml);

			aml = "<AML>";
			aml += "<Item type='Activity2' action='get'>";
			aml += "<id>"+this.getProperty("related_id","")+"</id>";
			aml += "</Item></AML>";
			Item itmActivity2 = inn.applyAML(aml);

			if(!itmActivity2.isError())
			{
				strName = itmActivity2.getProperty("name","");
			}

			sql = "UPDATE [In_WorkOrder_Detail] SET [keyed_name] = N'" + itmWorkOrder.getProperty("name","") + "-" + strName + "' WHERE [ID] = '" + this.getID() + "'";
			inn.applySQL(sql);		
		break;
		case "In_TimeRecord":
			aml = "<AML>";
			aml += "<Item type='Identity' action='get'>";
			aml += "<id>"+this.getProperty("owned_by_id","")+"</id>";
			aml += "</Item></AML>";
			Item itmIdentity = inn.applyAML(aml);

			DateTime dtNewDateTime = Convert.ToDateTime(this.getProperty("in_start_time",""));
			dtNewDateTime = dtNewDateTime.AddHours(8);
			string strDate = dtNewDateTime.ToString("yyyyMMdd");

			sql = "UPDATE [In_TimeRecord] SET [keyed_name] = N'" + itmIdentity.getProperty("name","") + "-" + strDate + "' WHERE [ID] = '" + this.getID() + "'";
			inn.applySQL(sql);
		break;
		default:
			string name = "name";
			string type = this.getType();
			XmlElement itTypeNode = CCO.Cache.GetItemTypeFromCache(ref type, ref name);
			string strItemTypeID = itTypeNode.GetAttribute("id");
			
			sql = "select name,keyed_name_order from [Property] where source_id='" + strItemTypeID + "' and keyed_name_order is not null order by keyed_name_order";
			Item itmProperties = inn.applySQL(sql);			
			string strKeyName = "";
			for(int i=0;i<itmProperties.getItemCount();i++){
				strKeyName += this.getProperty(itmProperties.getItemByIndex(i).getProperty("name",""),"") + " ";
			}

			strKeyName = strKeyName.Trim(' ');

			sql = "UPDATE [" + this.getType().Replace(" ","_") + "] SET [keyed_name] = N'" + strKeyName + "' WHERE [ID] = '" + this.getID() + "'";
			inn.applySQL(sql);
		break;
		
		
		
	}
	
    if(this.getType()=="In_WorkOrder_Detail")
    {
        strName = this.getProperty("in_name","");

        aml = "<AML>";
        aml += "<Item type='In_WorkOrder' action='get'>";
        aml += "<id>"+this.getProperty("source_id","")+"</id>";
        aml += "</Item></AML>";
        Item itmWorkOrder = inn.applyAML(aml);

        aml = "<AML>";
        aml += "<Item type='Activity2' action='get'>";
        aml += "<id>"+this.getProperty("related_id","")+"</id>";
        aml += "</Item></AML>";
        Item itmActivity2 = inn.applyAML(aml);

        if(!itmActivity2.isError())
        {
            strName = itmActivity2.getProperty("name","");
        }

        sql = "UPDATE [In_WorkOrder_Detail] SET [keyed_name] = N'" + itmWorkOrder.getProperty("name","") + "-" + strName + "' WHERE [ID] = '" + this.getID() + "'";
    inn.applySQL(sql);
    }

    if(this.getType()=="In_TimeRecord")
    {
        aml = "<AML>";
        aml += "<Item type='Identity' action='get'>";
        aml += "<id>"+this.getProperty("owned_by_id","")+"</id>";
        aml += "</Item></AML>";
        Item itmIdentity = inn.applyAML(aml);

        DateTime dtNewDateTime = Convert.ToDateTime(this.getProperty("in_start_time",""));
        dtNewDateTime = dtNewDateTime.AddHours(8);
        string strDate = dtNewDateTime.ToString("yyyyMMdd");

        sql = "UPDATE [In_TimeRecord] SET [keyed_name] = N'" + itmIdentity.getProperty("name","") + "-" + strDate + "' WHERE [ID] = '" + this.getID() + "'";
    inn.applySQL(sql);
    }

    if(this.getType()=="in_contract")
    {
        aml = "<AML>";
        aml += "<Item type='ItemType' action='get'>";
        aml += "<name>in_contract</name>";
        aml += "</Item></AML>";
        Item itmItemType = inn.applyAML(aml);

        aml = "<AML>";
        aml += "<Item type='Property' action='get' orderBy='keyed_name_order'>";
        aml += "<source_id>"+itmItemType.getID()+"</source_id>";
        aml += "<keyed_name_order condition='is not null'></keyed_name_order>";
        aml += "</Item></AML>";
        Item itmProperty = inn.applyAML(aml);

        string strKeyName = "";

        for(int i=0;i<itmProperty.getItemCount();i++){
            strKeyName += this.getProperty(itmProperty.getItemByIndex(i).getProperty("name",""),"") + " ";
        }

        strKeyName = strKeyName.Trim(' ');

        sql = "UPDATE [in_contract] SET [keyed_name] = N'" + strKeyName + "' WHERE [ID] = '" + this.getID() + "'";
        inn.applySQL(sql);
    }
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
	throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itemR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Updatekeyed_name' and [Method].is_current='1'">
<config_id>2C1FB905E8384ED8AD2C5964D6FC3713</config_id>
<name>In_Updatekeyed_name</name>
<comments>重組keyed_name</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
