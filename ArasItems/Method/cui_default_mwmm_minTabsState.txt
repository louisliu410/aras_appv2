var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onMinimizedTabsStateCommand) {
	aras.setPreferenceItemProperties('Core_GlobalLayout', null, {'core_tabs_state': 'tabs min'});
	menuFrame.onMinimizedTabsStateCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_minTabsState' and [Method].is_current='1'">
<config_id>3FFEA34734E34D94BE74892D5E6BD68A</config_id>
<name>cui_default_mwmm_minTabsState</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
