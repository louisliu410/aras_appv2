document.getElementById('item_link_aras_span').innerHTML = aras.getItemProperty(document.item, 'item_keyed_name');

if (!window.parent.document.getElementById('relationshipLayoutContainer')) {
	var url = 'relationships.html?db=' + aras.getDatabase() + '&ITName=History Container&itemID=' +
		document.item.getAttribute('id') + '&editMode=0&tabbar=1&toolbar=1&where=dialog';
	url = aras.getScriptsURL(url);
	document.getElementById('relationships_tmp').contentWindow.location.replace(url);
	parent.item = document.item;
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='historyContainerOnFormPopulated' and [Method].is_current='1'">
<config_id>C3ED6707056A45FB8E5EF46642F5C9A4</config_id>
<name>historyContainerOnFormPopulated</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
