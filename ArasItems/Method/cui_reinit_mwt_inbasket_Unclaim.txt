if (inArgs.isReinit) {
	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	var unclaimFlg = false;
	if (workerFrame && workerFrame.grid) {
		var rowID = workerFrame.grid.getSelectedId();
		if (rowID) {
			var activityType = workerFrame.grid.getUserData(rowID, 'assignmentType');
			if (activityType == 'workflow') {
				var lockType = workerFrame.grid.getUserData(rowID, 'lockType');
				var unclaimFlg = lockType == '1';
			}
		}
	}

	return {'cui_disabled': !unclaimFlg};
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_inbasket_Unclaim' and [Method].is_current='1'">
<config_id>E9B63A4FCF344C32A455D6981AD39DB9</config_id>
<name>cui_reinit_mwt_inbasket_Unclaim</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
