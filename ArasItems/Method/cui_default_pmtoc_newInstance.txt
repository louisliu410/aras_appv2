var topWindow = aras.getMostTopWindowWithAras(window);
var treeFrame = topWindow.tree;
if (treeFrame && treeFrame.ItemTypeInToc) {
	var itInToc = new treeFrame.ItemTypeInToc(inArgs.rowId);
	itInToc.createNewInstance();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmtoc_newInstance' and [Method].is_current='1'">
<config_id>3EA9FA2C3D934B3196E04CDD71453137</config_id>
<name>cui_default_pmtoc_newInstance</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
