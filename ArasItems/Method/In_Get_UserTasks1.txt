/*
目的:取得user_tasks1的內容,只取 代辦事項中類行為in_workorder的待辦事項
做法:
1.收到的任務(狀態為 active 的 mytask)
2.找到執行中的打卡派工單
3.
位置:
In_Report 的 Method
*/

//System.Diagnostics.Debugger.Break();
string strMethodName = "In_Get_UserTasks";

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;
string str_r = "";
Item rst=inn.newItem();

string strOutput1 = "";
string strOutput2 = "";
string strInput1 = this.getProperty("in_input1","");	//取得Identity id
string strInput2 = this.getProperty("in_input2","");

string strUserId = inn.getUserID();
string strIdentityId = inn.getUserAliases();


if(strInput1!="")
{
	Item itmUser = _InnH.GetUserByAliasIdentityId(strInput1);
	strUserId = itmUser.getID();
	strIdentityId = strInput1;
}
	
itmR = inn.newItem("tmp");
try
{
    DateTime dtNow = System.DateTime.Now;
    DateTime firstOfThisMonth = new DateTime(dtNow.Year, dtNow.Month, 1);
    DateTime firstOfNextMonth = firstOfThisMonth.AddMonths(1);
    DateTime lastOfThisMonth = firstOfNextMonth.AddSeconds(-1);

	//要抓這個人的其他打卡單據
	aml = "<AML>";
	aml += "<Item type='In_TimeRecord' action='get' orderBy='in_start_time DESC' select='id,in_workorder(in_wff_id),in_start_time'>";			
	aml += "<owned_by_id>" + strIdentityId + "</owned_by_id>";	
	aml += "<in_end_time condition='is null'></in_end_time>";			
	aml += "</Item></AML>";			
	Item itmOtherTimeRecords = inn.applyAML(aml);	
	string strWFFIds = "";
	string strStartTime = "";
	string strTimerecordId = "";
	for(int i=0;i<itmOtherTimeRecords.getItemCount();i++)
	{
		Item itmOtherTimeRecord = itmOtherTimeRecords.getItemByIndex(i);
		if(itmOtherTimeRecord.getProperty("in_workorder","")!="")
		{
			strStartTime = DateTime.Parse(itmOtherTimeRecord.getProperty("in_start_time")).ToString("HH:mm");
			strTimerecordId = itmOtherTimeRecord.getID();
			strWFFIds += itmOtherTimeRecord.getPropertyItem("in_workorder").getProperty("in_wff_id") + ",";
		}
			
	}
	strWFFIds = "," + strWFFIds;
	
	
	
	//收到的任務
	aml = "<AML>";
	aml += "<Item type='InBasket Task' action='get'>";
	aml += "<itemtype>321BD822949149C597FD596B1212B85C</itemtype>";
	aml += "<status>Active</status>";
	aml += "<my_assignment>1</my_assignment>";
	aml += "</Item>";
	aml += "</AML>";	
	Item itmMyTasks = inn.applyAML(aml);
	
	
	int inttask_count = 0;
	//收到的任務
	for(int i=0;i<itmMyTasks.getItemCount();i++)
	{
	    
		Item itmMyTask = itmMyTasks.getItemByIndex(i);
		itmMyTask.setProperty("due_date_short",itmMyTask.getProperty("due_date").Split('T')[0]);
		itmMyTask.setProperty("subject",itmMyTask.getPropertyAttribute("container","keyed_name"));		
		itmMyTask.setProperty("container_type",itmMyTask.getPropertyAttribute("container_type_id","keyed_name"));
		itmMyTask.setType("inbasket_task");
		Item workFlowForm=itmMyTask.getPropertyItem("item");
		workFlowForm.setAttribute("action","get");
		workFlowForm=workFlowForm.apply();
		string itmType=workFlowForm.getProperty("in_itemtype","");
		if(itmType=="In_WorkOrder"){
		    itmMyTask.setProperty("xitemtype",itmType);
		    itmMyTask.setProperty("is_workorder","1");
		    rst.addRelationship(itmMyTask);
			if(strWFFIds.Contains(workFlowForm.getID()))
			{
				rst.setProperty("current_workorder_keyed_name",workFlowForm.getProperty("in_keyed_name"));
				rst.setProperty("current_workorder_id",workFlowForm.getProperty("in_item_id"));
				rst.setProperty("current_workorder_starttime",strStartTime);
				rst.setProperty("timerecord_id",strTimerecordId);
				rst.setProperty("current_workorder_assid",itmMyTask.getID());
			}
			inttask_count++;
		}
	}
	
	if(rst.getProperty("current_workorder_keyed_name","")=="")
	{
		rst.setProperty("current_workorder_keyed_name","(無)");
		rst.setProperty("current_workorder_id","");
		rst.setProperty("current_workorder_starttime","(無)");
		rst.setProperty("timerecord_id","");
	}
	
	rst.setProperty("task_count",inttask_count.ToString());
	
	
	
	
	
	
	
	
	
	

}
catch(Exception ex)
{   
    //if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


    string strError = ex.Message + "\n";

    if(aml!="")
        strError += "無法執行AML:" + aml  + "\n";

    if(sql!="")
        strError += "無法執行SQL:" + sql  + "\n";

    string strErrorDetail="";
    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    _InnH.AddLog(strErrorDetail,"Error");
    strError = strError.Replace("\n","</br>");
    throw new Exception(_InnH.Translate(strError));
}


return rst;
//return inn.newResult(rst.dom.InnerXml);

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_UserTasks1' and [Method].is_current='1'">
<config_id>E64CFA45C0B34659BB144EBEE0D382D8</config_id>
<name>In_Get_UserTasks1</name>
<comments>只取 代辦事項中類行為in_workorder的待辦事項</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
