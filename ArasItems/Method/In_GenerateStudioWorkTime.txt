Innovator inn = this.getInnovator();

DateTime  WorkDate =  Convert.ToDateTime("2015-01-01 00:00:00");
string r="";
string aml = "";
string[] sections = new string[] { "上午", "下午", "晚上"};
for(int i=0;i<365;i++)
{
	for(int j=0;j<3;j++)
	{
		aml = "<AML>";
		aml += "<Item type='in_StudioWorkTime' action='add'>";
		aml += "<source_id>" + this.getID() + "</source_id>";
		aml += "<in_workdate>" + WorkDate.AddDays(i).ToString("yyyy-MM-ddTHH:mm:ss") + "</in_workdate>";
		aml += "<in_section>" + sections[j] + "</in_section>";
		aml += "</Item>";
		aml += "</AML>";
		Item tmp = inn.applyAML(aml);
		
		r+= WorkDate.AddDays(i).ToString("yyyy-MM-dd") + "-" + sections[j] + "<br>";
	}
}

return inn.newResult(r);


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GenerateStudioWorkTime' and [Method].is_current='1'">
<config_id>07FFEC9758A64756ACB3334492145AEF</config_id>
<name>In_GenerateStudioWorkTime</name>
<comments>Edit By Inno</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
