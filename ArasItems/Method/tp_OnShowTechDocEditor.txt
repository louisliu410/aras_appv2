return ModulesManager.using(['aras.innovator.core.ItemWindow/DefaultItemWindowView', 'aras.innovator.core.ItemWindow/DefaultItemWindowCreator'])
	.then(function(DefaultItemWindowView, Creator) {
		function TechDocItemWindowView(inDom, inArgs) {
			DefaultItemWindowView.call(this, inDom, inArgs);
		}
		TechDocItemWindowView.prototype = new DefaultItemWindowView();
		TechDocItemWindowView.prototype.getViewUrl = function() {
			return '/Modules/aras.innovator.TDF/TechDocContainerView';
		};
		TechDocItemWindowView.prototype.getWindowArguments = function() {
			var baseArgs = DefaultItemWindowView.prototype.getWindowArguments.call(this);
			baseArgs.reserveSpaceForSidebar = true;
			return baseArgs;
		};

		var view = new TechDocItemWindowView(inDom,inArgs);
		var creator = new Creator(view);
		return creator.ShowView();
	});

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='tp_OnShowTechDocEditor' and [Method].is_current='1'">
<config_id>DD3F0618FD104C369DBC4C3CCB407E14</config_id>
<name>tp_OnShowTechDocEditor</name>
<comments>Loads TechDoc editor</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
