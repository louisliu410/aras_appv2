var itemTypeId = this.getProperty("source_id");
	Item query = this.newItem("DiscussionTemplate", "get");
	query.setProperty("source_id", itemTypeId);
	query = query.apply();
	if (query.getItemCount() == 0)
	{
		Item result = DeleteCreateDiscussionDefinitionAction(itemTypeId);
		if (result.isError())
		{
			return result;
		}
		result = DeleteOnAfterCopyServerEvent(itemTypeId);
		if (result.isError())
		{
			return result;
		}	
	}

	return this;
}

private Item DeleteCreateDiscussionDefinitionAction(String itemTypeId)
{
	const string actionId = "5B155B40AD8F405D812EF95BC5848214"; // action "Create Discussion Definition"
	Item itemAction = this.newItem("Item Action", "delete");
	itemAction.setAttribute("where", "related_id = '" + actionId + "' and source_id = '" + itemTypeId + "'");
	return itemAction.apply();
}

private Item DeleteOnAfterCopyServerEvent(String itemTypeId)
{
	const string methodId = "22232D22D323449A936C1B30169D8602"; // method "VC_CopyDiscussionDefinition"
	Item serverEvent = this.newItem("Server Event", "delete");
	serverEvent.setAttribute("where", "related_id = '" + methodId + "' and source_id ='" + itemTypeId + "'");
	return serverEvent.apply();

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='VC_DeleteSSVCActionsAndEvents' and [Method].is_current='1'">
<config_id>C2B4415970F44A66B52DB445B9FDC8F2</config_id>
<name>VC_DeleteSSVCActionsAndEvents</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
