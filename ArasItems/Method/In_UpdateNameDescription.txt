/*
目的:組合成派工單的主旨
做法:
1.取第一個Act2得 name 變成主旨: 執行 xxxx 及其後續任務
*/
Innovator inn = this.getInnovator();
Item itmRelWorkOrderAct2 = this.getRelationships("In_WorkOrder Activity2");
if(itmRelWorkOrderAct2.isError())
	return this;
string strAct2Id = itmRelWorkOrderAct2.getItemByIndex(0).getProperty("related_id","");
Item itmAct2 = inn.getItemById("Activity2",strAct2Id);
//if(this.getProperty("classification","")=="執行流程")
//{
this.setProperty("name","執行 " + itmAct2.getProperty("name") + " 及其後續任務");
//}
//else
//{
//this.setProperty("name","更換 " + itmAct2.getProperty("name") + " 等任務的任務協辦人員");
//}
return this;



#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateNameDescription' and [Method].is_current='1'">
<config_id>B98FA2E7E2764D89BEDE68E09B06FA12</config_id>
<name>In_UpdateNameDescription</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
