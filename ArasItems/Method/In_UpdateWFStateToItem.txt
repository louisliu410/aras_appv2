//System.Diagnostics.Debugger.Break(); 
Innovator inn = this.getInnovator();
Item controlledItem = this.apply("Get Controlled Item");

string AttachedItemType = "In_Drug_Lic";
string AttachedItemRelType = "In_Drug_IAP_Drug_Lic"; 
string StatePropertyName_AttItem = "in_workflow_state"; //Drug Approval
string StatePropertyName_ConItem = "in_workflow_state"; //IAP

string CurrentActName = this.getProperty("label");

string SQLTypeName_ConItem = controlledItem.getType();
SQLTypeName_ConItem = SQLTypeName_ConItem.Replace(" ","_");

string SQL_ConItem = "update  [" + SQLTypeName_ConItem.Replace(" ","_") + "] set " + StatePropertyName_ConItem + "='" + CurrentActName + "' where [id]='" + controlledItem.getID() + "'";
inn.applySQL(SQL_ConItem);

string SQL_AttItem = "";
Item AttItems = inn.newItem(AttachedItemRelType,"get");
AttItems.setProperty("source_id",controlledItem.getID() );
AttItems.setAttribute("select","id,related_id");
AttItems = AttItems.apply();


//Item RelItems = controlledItem.getRelationships(AttachedItemRelType);
for(int i=0;i<AttItems.getItemCount();i++)
{
	SQL_AttItem = "update  [" + AttachedItemType + "] set " + StatePropertyName_AttItem + "='" + CurrentActName + "' where [id]='" + AttItems.getItemByIndex(i).getProperty("related_id") + "'";
	inn.applySQL(SQL_AttItem);
}

return this;




#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_UpdateWFStateToItem' and [Method].is_current='1'">
<config_id>706B270FE77E4DEB8CCA6900B3CAF136</config_id>
<name>In_UpdateWFStateToItem</name>
<comments>inn</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
