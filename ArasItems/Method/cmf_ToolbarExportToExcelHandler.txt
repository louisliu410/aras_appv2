var docEditor = window.cmfContainer.qpWindow.documentEditor;
if (docEditor) {
	docEditor.exportToExcel();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cmf_ToolbarExportToExcelHandler' and [Method].is_current='1'">
<config_id>6E96127815CF4033AF7C4948C2BC20B7</config_id>
<name>cmf_ToolbarExportToExcelHandler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
