/*
目的:
A.公司別必須與對應的使用者相同
B.一個Identity不得屬於多個組織
C.更新欄位(Identity Structure,Member)
位置:AfterUpdate/AfterAdd
做法:
B-1.取得Identity的關聯名稱
B-2.依序找出Identity的related_id
B-3.判斷這些related_id是否存在別的Member中
C-1.將所有in_dept為本身的identity都先清空,因為有可能部門被裁撤,變成孤兒
*/

//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);

string aml = "";
string sql = "";
string strErr = "";
string strRelationshipTypeNames = "";

Item itmApplyMethod = null;
Item itmIdentities=null;

try{
		//A.公司別必須與對應的使用者相同
		if(this.getProperty("is_alias","")=="1")
		{
			if(this.getProperty("in_user","")=="")
			{
				//代表可能是新增來的,所以還沒有值,就直接忽略				
			}
			else
			{			
				Item itmUser = inn.getItemById("User",this.getProperty("in_user",""));
				if(itmUser.getProperty("in_company","") != this.getProperty("in_company",""))
				{						
					strErr ="對應使用者["+itmUser.getProperty("keyed_name","")+"]的所屬公司別為["+itmUser.getProperty("in_company","空白")+"],不可與本角色的所屬公司別相異";
				}
			}
		}
		
		//C-1.將所有in_dept為本身的identity都先清空,因為有可能部門被裁撤,變成孤兒
		sql = "Update [Identity] set in_dept=null where in_dept='" + this.getID() + "'";
		inn.applySQL(sql);
		
		//B.一個Identity不得屬於多個組織
		if(this.getProperty("in_is_org","")=="1")		
		{        
			//B-1.取得Identity的關聯名稱
			 //itmApplyMethod = inn.applyMethod("In_getObjectAttributeValue","<HeadObject>ItemType</HeadObject><HeadCondition>name</HeadCondition><HeadContent>Identity</HeadContent><BodyObject>RelationshipType</BodyObject><ObjectAttributeName>name</ObjectAttributeName>");
			
			//strRelationshipTypeNames = itmApplyMethod.getProperty("ObjectAttributeValue","");
			//string[] strRelationshipTypeName = strRelationshipTypeNames.Split(',');
			string[] strRelationshipTypeName = {"Identity Structure","Member"};
			for(int i=0;i<strRelationshipTypeName.Length;i++){
				aml = "<AML>";
				aml += "<Item type='"+strRelationshipTypeName[i]+"' action='get'>";
				aml += "<source_id>" + this.getID() + "</source_id>";
				aml += "</Item></AML>";
				itmIdentities = inn.applyAML(aml);
				
				//B-2.依序找出Identity的related_id
				for(int j=0;j<itmIdentities.getItemCount();j++){
					Item itmIdentity = itmIdentities.getItemByIndex(j);
					
					//B-3.判斷這些related_id是否存在別的Member中
					aml = "<AML>";
					aml += "<Item type='"+itmIdentity.getType()+"' action='get'>";
					aml += "<source_id>";
					aml += "<Item type='Identity' action='get'>";
					aml += "<in_is_org>1</in_is_org>";
					aml += "<id condition='ne'>" + this.getID() + "</id>";
					aml += "</Item>";
					aml += "</source_id>";
					aml += "<related_id>" + itmIdentity.getProperty("related_id","") + "</related_id>";
					aml += "</Item></AML>";
					Item itmOrgMember = inn.applyAML(aml);
					if(!itmOrgMember.isError()){
						strErr +="["+itmIdentity.getPropertyAttribute("related_id","keyed_name","")+"]已屬於["+itmOrgMember.getItemByIndex(0).getPropertyAttribute("source_id","keyed_name","")+"]部門,不可再加入本部門"+",";
					}
				}
			}
		}
        
    if(strErr!="")
        throw new Exception(_InnH.Translate(strErr));
}
catch(Exception ex){
    if (PermissionWasSet)
        Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);
        throw new Exception(ex.InnerException==null?ex.Message:ex.InnerException.Message);
}
if(PermissionWasSet)Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ValidateIdentity' and [Method].is_current='1'">
<config_id>211324FABC04412BBF8447E26F59B23D</config_id>
<name>In_ValidateIdentity</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
