//目的:將Document的過程Word與完稿PDF清空
//位置:In_Drug_Lic_Deliverable Relationship 上

Innovator inn = this.getInnovator();
Item itmDocument = this.getRelatedItem();
itmDocument.setProperty("in_word_file","");
itmDocument.setProperty("in_modify_file","");

itmDocument = itmDocument.apply("edit");
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ClearIn_Word' and [Method].is_current='1'">
<config_id>C6B3497175724476AF582852CCB44B9A</config_id>
<name>In_ClearIn_Word</name>
<comments>Drug</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
