/*
目的:秀出該版本物件的簽核紀錄
位置:Action
做法:
1.取得in_wff_id
2.top.aras.uiShowItemEx 顯示出 SignoffForm
*/
var inn = this.getInnovator();
var in_wff_id = this.getProperty("in_wff_id","");
if(in_wff_id==="")
{
	alert(inn.applyMethod("In_Translate","<text>目前無簽核紀錄</text>").getResult());
	
	return;
}

var itmInWFF = inn.newItem("In_WorkflowForm", "get");
itmInWFF.setID(in_wff_id);
itmInWFF = itmInWFF.apply();

var param = top.aras.newObject();
param.title =inn.applyMethod("In_Translate","<text>簽核紀錄</text>").getResult() ;
param.formId = "41449A928F3341149788B4A17F164845"; //Workflow Signoffs
param.aras = top.aras;
param.item = itmInWFF;
var options = {
	dialogWidth: 800,
	dialogHeight: 385,
	resizable: 'no'
};
var res = top.aras.modalDialogHelper.show('DefaultPopup', window, param, options, 'ShowFormAsADialog.html');







#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ShowSignOff' and [Method].is_current='1'">
<config_id>3B6B745CA395436BB38BCF9B6AB096EB</config_id>
<name>In_ShowSignOff</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
