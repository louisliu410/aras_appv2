var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onDownloadCommand) {
	workerFrame.onDownloadCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwmm_onDownload' and [Method].is_current='1'">
<config_id>7701B40D9FA8474A85849A6E5E5711AE</config_id>
<name>cui_default_mwmm_onDownload</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
