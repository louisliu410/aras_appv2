/*
目的:挑選L2(預算階)的會計科目
*/
var retObj = new Object();

if(thisItem.getProperty("in_company","") === ""){
    alert(this.getInnovator().applyMethod("In_Translate","<text>請先選擇公司別</text>").getResult());
    retObj["in_company"] = {filterValue:0,isFilterFixed:true};
    retObj["in_accounting_level"] = {filterValue:0,isFilterFixed:true};
}else{
    retObj["in_accounting_level"] = {filterValue:2,isFilterFixed:true};
    retObj["in_company"] = {filterValue:thisItem.getProperty("in_company",""),isFilterFixed:true};
}
return retObj;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_cfg_PickL2AccountItem' and [Method].is_current='1'">
<config_id>181EBFCFE4DE40B18C543ECAD7454C76</config_id>
<name>In_cfg_PickL2AccountItem</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
