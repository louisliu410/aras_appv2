/*
目的:清除交付物的in_project資訊
位置:project before delete
*/
//System.Diagnostics.Debugger.Break();
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";


Item itmR = this;
	
try
{
    //取得這個專案所有的Deliverables
	//1.Project Doc
	//2.Activity2 Deliverable

	Item itmProjTree =inn.newItem("Project","select_project_tree");
	itmProjTree.setID(this.getID());
	itmProjTree = itmProjTree.apply();

	string Act2AndWbsIds = "";
	for(int i=0;i<itmProjTree.getItemCount();i++)
	{
		Item itmElement = itmProjTree.getItemByIndex(i);
		Act2AndWbsIds += "'" + itmElement.getID() + "',";
	}
	Act2AndWbsIds = Act2AndWbsIds.TrimEnd(',');
	aml = "<AML>";
	aml += "<Item type='Activity2 Deliverable' action='get' select='related_id(id,itemtype)'>";
	aml += "<source_id condition='in'>" + Act2AndWbsIds + "</source_id>";
	aml += "</Item></AML>";
	Item itmActDels = inn.applyAML(aml);

	aml = "<AML>";
	aml += "<Item type='Project Docs' action='get' select='related_id(id,itemtype)'>";
	aml += "<source_id>" + this.getID() + "</source_id>";
	aml += "</Item></AML>";
	Item itmProjectDocs =  inn.applyAML(aml);
	
	Item itmDel=null;
	for(int i=0;i<itmActDels.getItemCount();i++)
	{
		Item itmActDel = itmActDels.getItemByIndex(i);
		itmDel = itmActDel.getPropertyItem("related_id");
		if(itmDel==null)
		    continue;
		sql = "Update [" + itmDel.getType().Replace(" ","_") + "] set in_project=null, team_id=null where id='" + itmDel.getID() + "'";
		inn.applySQL(sql);
	}
	
	for(int i=0;i<itmProjectDocs.getItemCount();i++)
	{
		Item itmProjectDoc = itmProjectDocs.getItemByIndex(i);
		itmDel = itmProjectDoc.getPropertyItem("related_id");
		if(itmDel==null)
		    continue;
		sql = "Update [" + itmDel.getType().Replace(" ","_") + "] set in_project=null, team_id=null  where id='" + itmDel.getID() + "'";
		inn.applySQL(sql);
	}
}
catch(Exception ex)
{	
	if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strMethodName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
	strMethodName = strMethodName.Replace("Aras.IOM.Item methodCode(Aras.Server.Core.","");
	strMethodName = strMethodName.Replace("EventArgs)","");
	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";
	//strError += (ex.InnerException==null?"":ex.InnerException.Message + "\n");

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";
		
	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	Innosoft.InnUtility.AddLog(strErrorDetail,"Error");
	//return inn.newError(_InnH.Translate(strError));
	throw new Exception(_InnH.Translate(strError));
}
if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_RemoveDeliProjInfo' and [Method].is_current='1'">
<config_id>ADBC5C07DF2A405289029163CFA6FB53</config_id>
<name>In_RemoveDeliProjInfo</name>
<comments>清除交付物的in_project資訊</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
