/*
目的:依據ID取得物件的 MergeAML
位置:
scripts\propsDialog.html
*/
//System.Diagnostics.Debugger.Break();
//throw new Exception("aa");
string strItemtype = this.getProperty("type");
string strItemId = this.getProperty("id");
string strAction = this.getProperty("action");
string strActionType = this.getProperty("action_type","getaml"); // getaml, getids
string strTmp = "";
int intLevel = Convert.ToInt32(this.getProperty("level","0"));
string strItemAML="";
Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
Item itmR  = null;
switch(strActionType)
{
    case "getaml":
        itmR = _InnH.GetCurrentItemById(strItemtype,strItemId,intLevel,strAction);
        switch(strItemtype)
        {
            case "Method":
                itmR.removeAttribute("id");
                itmR.setAttribute("action","in_merge");
                itmR.setAttribute("where","[method].[name]='" + itmR.getProperty("name") + "' and [" + itmR.getType().Replace(" ","_") + "].is_current='1'");
                break;
            default:			
                //如果是relationship則須修改where條件成 srouce_id, related_id
				string name = "name";
				string type = strItemtype;
				XmlElement itTypeNode = CCO.Cache.GetItemTypeFromCache(ref type, ref name);
				string strItemIsRel = itTypeNode.GetAttribute("is_relationship");
				string strWhere="[" + itmR.getType().Replace(" ","_") + "].config_id='" + itmR.getProperty("config_id","") + "' and [" + itmR.getType().Replace(" ","_") + "].is_current='1'";
				if(strItemIsRel=="1")
				{
					string strRelCriteria="";
					string strSourceCriteria=" [" + itmR.getType().Replace(" ","_") + "].source_id='" + itmR.getProperty("source_id","") + "' ";
					if(itmR.getProperty("related_id","")!="")
					{
						 strRelCriteria = " and [" + itmR.getType().Replace(" ","_") + "].related_id=(select id from [" + itmR.getPropertyAttribute("related_id","type").Replace(" ","_") + "] ";
						strRelCriteria += " where [" + itmR.getPropertyAttribute("related_id","type").Replace(" ","_") + "].keyed_name='" + itmR.getPropertyAttribute("related_id","keyed_name") + "'";
						strRelCriteria += "  and [" + itmR.getPropertyAttribute("related_id","type").Replace(" ","_") + "].is_current='1'))";
						
						Item itmRel = itmR.getRelatedItem();
						Item itmNewRel = inn.newItem(itmRel.getType(),"get");
						itmNewRel.setProperty("keyed_name",itmRel.getProperty("keyed_name",""));
						
						itmR.setPropertyItem("related_id",itmNewRel);
					}
					strWhere = strSourceCriteria + strRelCriteria;
				}
                itmR.setAttribute("action","in_merge");
                itmR.removeAttribute("id");
                itmR.setAttribute("where",strWhere);
				
                break;
        }

		

		
        break;
    case "getids":
        strTmp = "ItemTypeName:" + strItemtype + "\n";
        strTmp += "IDs:" + strItemId;

		string[] arrIDs = strItemId.Split(',');
		strTmp += "\n<AML>";
		for(int i=0;i<arrIDs.Length;i++)
		{
			itmR = _InnH.GetCurrentItemById(strItemtype,strItemId,intLevel,strAction);
		    itmR.setAttribute("action","edit");
		     itmR.removeAttribute("id");
            //itmR.setAttribute("id",itmR.getProperty("config_id",""));
            itmR.setAttribute("where","config_id='" + itmR.getProperty("config_id","") + "' and is_current='1'");
			strTmp += itmR.dom.SelectSingleNode("//Result").InnerXml;
		}
		strTmp += "</AML>";


        itmR  = inn.newResult(strTmp);
        break;
}
return itmR;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetItemAML' and [Method].is_current='1'">
<config_id>A0780996AE124B3E942080A7E10BB689</config_id>
<name>In_GetItemAML</name>
<comments>inn tool</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
