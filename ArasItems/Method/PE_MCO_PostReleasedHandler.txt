Item result = this.apply("Set Release Date");

if(result.isError())
{
  return result;
}

Item affParts = this.newItem("Simple MCO Part", "get");
affParts.setProperty("source_id", this.getID());

affParts = affParts.apply();

int count = affParts.getItemCount();
if(count < 0)
{
  return affParts;
}
else
{
  for(int i = 0; i < count; i++)
  {
    Item affPart = affParts.getItemByIndex(i);
    result = affPart.apply("PE_update_has_change_pending");
    if(result.isError())
    {
      return result;
    }
  }
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_MCO_PostReleasedHandler' and [Method].is_current='1'">
<config_id>7A444A33207540829A30069E738522A1</config_id>
<name>PE_MCO_PostReleasedHandler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
