If Not IsNothing(Me.node) Then
  Dim inn As Innovator = Me.getInnovator()
  Dim idenList As String = inn.getUserAliases()
  Dim m As String = Me.getProperty("managed_by_id")
  If Not IsNothing(m) AndAlso Not IsNothing(idenList) AndAlso idenList.indexOf(m)>-1 Then
    Dim propColor As String = "#FFCCCC"
    Dim cssPropNm As String = "fed_css"
    Dim props() As String = {"percent_compl", "date_start_act"}
    Dim b As New StringBuilder
    Dim i As Integer
    For i=0 To props.Length-1
      b.Append(String.Format(".{0}{{background-color:{1}}}{2}", props(i), propColor, vbNewLine))
    Next i
    Me.setProperty(cssPropNm, b.toString())
  End If
End If

return Me
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PM_setFedCss_For_ACW' and [Method].is_current='1'">
<config_id>219EFC72C91E4C15B53F2511A9CD7BDE</config_id>
<name>PM_setFedCss_For_ACW</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>VB</method_type>
</Item>
</AML>
