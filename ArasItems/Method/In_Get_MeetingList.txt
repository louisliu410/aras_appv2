//本方法主要提供MeetingUserLogin所需的資料，由b.aspx使用。


Innovator inn=this.getInnovator();
Item meetings=inn.newItem();
Item rst=inn.newItem();
//System.Diagnostics.Debugger.Break();
meetings.setAttribute("type","In_Meeting");
meetings.setAttribute("action","get");


meetings=meetings.apply();
int mCount=meetings.getItemCount();
for(int i =0;i<mCount;i++){
    //取出各個物件並修改其中的in_date_s/in_date_e格式，再加入到回傳(rst)中。
    //2017/9/21 追加:增加inn_incharge傳送單場負責人的名字
    Item tmp=meetings.getItemByIndex(i);
    try{
        string commander=tmp.getPropertyAttribute("owned_by_id","keyed_name");
        string date_s =System.DateTime.Parse(tmp.getProperty("in_date_s")).ToString("yyyy-MM-dd");
        string date_e =System.DateTime.Parse(tmp.getProperty("in_date_e")).ToString("yyyy-MM-dd");
        tmp.setProperty("in_date_s",date_s);
        tmp.setProperty("in_date_e",date_e);
        tmp.setProperty("inn_incharge",commander);
        rst.addRelationship(tmp);
        
    }catch(Exception ex){
        tmp.setProperty("in_date_s","no_data");
        tmp.setProperty("in_date_e","no_data");
        rst.addRelationship(tmp);

    }
   
    

}




return rst;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_MeetingList' and [Method].is_current='1'">
<config_id>CCA300F74B924ADFABAFEF1FE09C784A</config_id>
<name>In_Get_MeetingList</name>
<comments>取得會議清單，主要提供MeetingUserLogin需要的資料。由b.aspx使用。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
