/*
目的:取得user_tasks的內容
做法:
1.未完成的發出任務總數(workflow process建立者是自己,且流程狀態為Active)
2.草稿狀態文件數(抓取所有有流程機制的單據,加上狀態為 preminary start的 controlled 物件)
3.收到的任務(狀態為 active 的 mytask)



*/

//System.Diagnostics.Debugger.Break();
string strMethodName = "In_Get_UserTasks";

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;
string str_r = "";
Item rst=inn.newItem();

string strOutput1 = "";
string strOutput2 = "";
string strInput1 = this.getProperty("in_input1","");	//取得Identity id
string strInput2 = this.getProperty("in_input2","");

string strUserId = inn.getUserID();
string strIdentityId = inn.getUserAliases();


if(strInput1!="")
{
	Item itmUser = _InnH.GetUserByAliasIdentityId(strInput1);
	strUserId = itmUser.getID();
	strIdentityId = strInput1;
}
	
itmR = inn.newItem("tmp");
try
{
    DateTime dtNow = System.DateTime.Now;
    DateTime firstOfThisMonth = new DateTime(dtNow.Year, dtNow.Month, 1);
    DateTime firstOfNextMonth = firstOfThisMonth.AddMonths(1);
    DateTime lastOfThisMonth = firstOfNextMonth.AddSeconds(-1);

	//收到的任務
	aml = "<AML>";
	aml += "<Item type='InBasket Task' action='get'>";
	aml += "<itemtype>321BD822949149C597FD596B1212B85C</itemtype>";
	aml += "<status>Active</status>";
	aml += "<my_assignment>1</my_assignment>";
	aml += "</Item>";
	aml += "</AML>";	
	Item itmMyTasks = inn.applyAML(aml);
	
	
	//未完成的發出任務
	aml = "<AML>";
	aml += "<Item type='workflow process' action='get' select='in_keyedname,in_itemtype,in_item_id,in_itemtype_label,in_current_activity_id,in_current_activity,in_config_id'>";
	aml += "<state>Active</state>";
	aml += "<created_by_id>" + strUserId + "</created_by_id>";
	aml += "</Item>";
	aml += "</AML>";	
	Item itmOutTasks = inn.applyAML(aml);
	
	//抓取所有有流程機制的單據
	aml = "<AML>";
	aml += "<Item type='Allowed Workflow' action='get' select='source_id(name,label)' >";
	aml += "<source_id condition='ne'>FAC120F9746749EA996D1C84E2C05BF7</source_id>";
	aml += "</Item></AML>";
	Item itmWorkflowItems = inn.applyAML(aml);
	Dictionary<string, string> ditDraftItemtype = new Dictionary<string, string>( );
	
	for(int i=0;i<itmWorkflowItems.getItemCount();i++)
	{
		Item itmSourceItem  =itmWorkflowItems.getItemByIndex(i).getPropertyItem("source_id");
		if(!ditDraftItemtype.ContainsKey(itmSourceItem.getProperty("name","")))
		{
			ditDraftItemtype.Add(itmSourceItem.getProperty("name",""),itmSourceItem.getProperty("label",""));
		}
	}
	
	Item itmDraftItems = null;

	foreach (KeyValuePair<string, string> itmDraftItemtype in ditDraftItemtype)
	{
		//草稿狀態派工單
		aml = "<AML>";
		aml += "<Item type='" + itmDraftItemtype.Key + "' action='get'>";
		aml += "<or>";
		aml += "<owned_by_id>" + strIdentityId + "</owned_by_id>";
		aml += "<created_by_id>" + strUserId + "</created_by_id>";
		aml += "</or>";
		aml += "<or>";
		aml += "<state>Preliminary</state>";
		aml += "<state>Start</state>";
		aml += "<state>Initial</state>";
		aml += "<state>New</state>";
		aml += "</or>";
		aml += "<or>";
		aml += "	<in_wfp_state condition='is null'/>";
		aml += "	<in_wfp_state></in_wfp_state>";
		aml += "</or>";
		aml += "</Item></AML>";	
		Item itmDraftItem = inn.applyAML(aml);
		if(itmDraftItem.isError())
		    continue;
		if(itmDraftItem.getItemCount()==0)
		    continue;
		if(itmDraftItems==null)
			itmDraftItems = itmDraftItem;
		else
			itmDraftItems.appendItem(itmDraftItem);
	}
	aml="1";
	
	//未完成的發出任務總數
	//草稿狀態文件數
	int intDraftCount=0;
	if(itmDraftItems!=null)
	 intDraftCount= itmDraftItems.getItemCount();
	aml="2";
	
	int intOutTaskCount=0;
	if(!itmOutTasks.isError())
	 intOutTaskCount= itmOutTasks.getItemCount();
	rst.setProperty("start_doc_count",intDraftCount.ToString());
	rst.setProperty("active_out_task_count",intOutTaskCount.ToString());
	
	aml="3";
	//正常的任務數	
    rst.setProperty("task_count",itmMyTasks.getItemCount().ToString());
	
	aml="4";
	//收到的任務
	if(!itmMyTasks.isError())
	{
		for(int i=0;i<itmMyTasks.getItemCount();i++)
		{
			
			Item itmMyTask = itmMyTasks.getItemByIndex(i);
			itmMyTask.setType("inbasket_task");
			itmMyTask.setProperty("due_date_short",itmMyTask.getProperty("due_date").Split('T')[0]);
			itmMyTask.setProperty("start_date_short",itmMyTask.getProperty("start_date").Split('T')[0]);
			//itmMyTask.setProperty("subject",itmMyTask.getPropertyAttribute("container","keyed_name"));		
			//itmMyTask.setProperty("container_type",itmMyTask.getPropertyAttribute("container_type_id","keyed_name"));
			itmMyTask.removeProperty("item_type_id");//因為如果是In_WorkflowForm,還要去轉換成in_itemtype的typeid ,浪費資源
			//if(itmMyTask.getPropertyAttribute("item","type")=="In_WorkflowForm")
			//{
				Item itmWFP = inn.getItemById("Workflow Process",itmMyTask.getProperty("container",""));
				itmMyTask.setProperty("item",itmWFP.getProperty("in_item_id",""));
				itmMyTask.setPropertyAttribute("item","keyed_name",itmWFP.getProperty("keyed_name",""));
				itmMyTask.setPropertyAttribute("item","type",itmWFP.getProperty("in_itemtype",""));	
				itmMyTask.setProperty("itemtype_label",itmWFP.getProperty("in_itemtype_label",""));				
				itmMyTask.setProperty("in_wff_id",itmWFP.getProperty("in_wff_id",""));				
			itmMyTask.setProperty("name",itmWFP.getProperty("in_current_activity",""));				
			//}
			rst.addRelationship(itmMyTask);
		}
	}
	
	
	
	aml="5";
	//發出的任務
	if(!itmOutTasks.isError())
	{
		//發出的任務
		for(int i=0;i<itmOutTasks.getItemCount();i++)
		{	    
			Item itmOutTask = itmOutTasks.getItemByIndex(i);
			//itmOutTask.setProperty("subject",itmOutTask.getProperty("keyed_name",""));
			//itmOutTask.setProperty("container_type",itmOutTask.getProperty("in_itemtype_label"));
			
			//itmOutTask.setProperty("in_item_cfgid",itmOutTask.getProperty("in_config_id",""));
			
			itmOutTask.setType("out_tasks");
			rst.addRelationship(itmOutTask);
		}
	}
	
	aml="6";
	if(itmDraftItems!=null)
	{
		for(int i=0;i<itmDraftItems.getItemCount();i++)
		{
			Item itmDraftItem = itmDraftItems.getItemByIndex(i);
			itmDraftItem.setProperty("in_itemtype_label",ditDraftItemtype[itmDraftItem.getType()]);		
			itmDraftItem.setProperty("in_itemtype",itmDraftItem.getType());	
			itmDraftItem.setType("start_doc");
			rst.addRelationship(itmDraftItem);
		}
		
	}
	
	
	
	
	
	
	rst.setType("method");
	rst = rst.apply("In_AppendExtraProperties");
	
	

}
catch(Exception ex)
{   
    //if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


    string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


    string strError = ex.Message + "\n";

    if(aml!="")
        strError += "無法執行AML:" + aml  + "\n";

    if(sql!="")
        strError += "無法執行SQL:" + sql  + "\n";

    string strErrorDetail="";
    strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
    _InnH.AddLog(strErrorDetail,"Error");
    strError = strError.Replace("\n","</br>");
    throw new Exception(_InnH.Translate(strError));
}


return rst;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_UserTasks' and [Method].is_current='1'">
<config_id>7CAFD3B20C484C93B7E73F70B6EE1870</config_id>
<name>In_Get_UserTasks</name>
<comments>取得供UserTasks.html使用的資料，由b.aspx 或 c.aspx呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
