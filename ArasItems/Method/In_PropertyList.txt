/*
目的:將Property變成下拉選單
位置:In_Sequence_Setting_Rules 的 in_ref_itemtype_list Property OnEditStart Event
*/

debugger;

var field = grid.columns_Experimental.get(colNumber, "name");
//var aml = "";
//aml = "<AML>";
//aml += "<Item type='Property' action='get'>";
//aml += "<source_id>43820D4178A24838985E139723333782</source_id>";
//aml += "</Item></AML>";
//
//var itmProps = top.aras.applyAML(aml);

//var itmProps = inn.applyAML(aml);

var labs = [], vals = [];
//for (var i=0; i<itmProps.getItemCount(); i++) {
for (var i=0; i<2; i++) {
//	var itmProp = itmProps.getItemByIndex(i);
//	vals.push(top.aras.getItemProperty(itmProp, 'id'));
//	var label = top.aras.getItemProperty(itmProp, 'label');
//	if(label=='')
//		top.aras.getItemProperty(itmProp, 'name');
//	labs.push(top.aras.getItemProperty(itmProp, label));
	vals.push(i);
	labs.push(i);
}


if (vals.indexOf("") === -1) {
	vals.unshift("");
	labs.unshift("");
}

grid.columns_Experimental.set(field, "editType", "FilterComboBox");
grid.columns_Experimental.set(field, "comboList", vals, labs);
return true;

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_PropertyList' and [Method].is_current='1'">
<config_id>78D48E727DEB4D50A56632861A6A3551</config_id>
<name>In_PropertyList</name>
<comments>inn core</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
