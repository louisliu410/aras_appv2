string cl = this.getProperty("classification");
if (cl == "")
{
	//Team identity's team_role must be a Team Identity
	Item q = this.newItem();
	q.loadAML("<Item type='Team Identity' action='get' select='source_id'><team_role>" + this.getID() + "</team_role></Item>");
	q = q.apply();
	int itemsCount = q.getItemCount();
	if (itemsCount == 1)
		return this.getInnovator().newError("Classification could not be changed. This identity is used in Team '" + q.getPropertyAttribute("source_id", "keyed_name") + "'.");
	else if (itemsCount > 1)
		return this.getInnovator().newError("Classification could not be changed. This identity is used in multiple Teams.");
}
else if (cl == "Team")
{
	//Member's related_id identity must not be a Team Identity.
	Item q = this.newItem();
	q.loadAML("<Item type='Member' action='get' select='source_id'><related_id>" + this.getID() + "</related_id></Item>");
	q = q.apply();
	if (q.getItemCount() == 1)
		return this.getInnovator().newError("Classification could not be changed. This identity is used in Identity '" + q.getPropertyAttribute("source_id", "keyed_name") + "'.");
	else if (q.getItemCount() > 1)
		return this.getInnovator().newError("Classification could not be changed. This identity is used in multiple Identities.");
}

string is_alias = this.getProperty("is_alias");
// If one of properties was not set try to get it from database
if (this.getAttribute("action") != "add" && this.getAttribute("action") != "create")
{
	if (cl == null && is_alias != null)
	{
		Item q = this.newItem("Identity", "get");
		q.setID(this.getID());
		q.setAttribute("select", "classification");
		q = q.apply();
		if (q.isError())
			return q;
		else
			cl = q.getProperty("classification");
	}
	else if (is_alias == null && cl != null)
	{
		Item q = this.newItem("Identity", "get");
		q.setID(this.getID());
		q.setAttribute("select", "is_alias");
		q = q.apply();
		if (q.isError())
			return q;
		else
			is_alias = q.getProperty("is_alias");
	}
}

//Check if Identity has not set both classification="Team" and is_alias="1"
if (cl == "Team" && is_alias == "1")
{
	return this.getInnovator().newError("Team Identity could not has is_alias='1'.");
}
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='Identity_IsClassificationValid' and [Method].is_current='1'">
<config_id>1B0D1B285D3B417590D3986110AD129E</config_id>
<name>Identity_IsClassificationValid</name>
<comments>Check if related Identity is not a Team Identity</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
