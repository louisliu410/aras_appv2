var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onEditCommand) {
	workerFrame.onEditCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onEditCommand' and [Method].is_current='1'">
<config_id>D047B0DF0E684358B69003412AC40AC3</config_id>
<name>cui_default_mwt_onEditCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
