/*
目的:產生每日的專案工時審核表(In_Timesheet_Auditing)
做法:
1.搜尋所有的專案工時審核表
2.刪除全部的關聯:工時審核-一天前(In_Timesheet_R1),工時審核-二天前(In_Timesheet_R2),工時審核-三天前(In_Timesheet_R3)
3.搜尋一天前,二天前,三天前各專案的工時紀錄表(In_TimeRecord),且生命週期狀態為In Review
4.將超過三天的專案工時審核表一律寫入核准(approved)
5.將工時紀錄表分別增加至各關聯:工時審核-今天,工時審核-昨天,工時審核-前天
位置:
Run Server Method
*/

//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();

DateTime dtNowDateTime = DateTime.Now;

string strNowDateTime = dtNowDateTime.ToString("yyyy-MM-ddTHH:mm:ss");
string aml = "";
string sql = "";
string param = "";
string strWorkDays = "";

//工作日
Item itmApplyMethod = inn.applyMethod("In_GetWorkDays","<BasisDay>"+strNowDateTime+"</BasisDay><Days>-3</Days><Interval>-1</Interval>");
strWorkDays = itmApplyMethod.getProperty("workdays","");
string[] strWorkDay = strWorkDays.Split(',');

//1.搜尋所有的專案工時審核表
aml = "<AML>";
aml += "<Item type='In_Timesheet_Auditing' action='get'>";
aml += "</Item></AML>";
Item itmTimesheetAuditings = inn.applyAML(aml);
for(int i=0;i<itmTimesheetAuditings.getItemCount();i++){
	Item itmTimesheetAuditing = itmTimesheetAuditings.getItemByIndex(i);
	
	//更新工作日(in_workdate)
	sql = "Update [innovator].[In_Timesheet_Auditing] set ";
	sql += "[in_workdate] = '" + "一天前:" + strWorkDay[0] + "'+char(10)+'" + "二天前:" + strWorkDay[1] + "'+char(10)+'" + "三天前:" + strWorkDay[2] + "'";
	sql += " where id = '" + itmTimesheetAuditing.getID() + "'";
	inn.applySQL(sql);
	
	//2.刪除全部的關聯:工時審核-一天前(In_Timesheet_R1),工時審核-二天前(In_Timesheet_R2),工時審核-三天前(In_Timesheet_R3)
	for(int j=1;j<=3;j++){
		aml = "<AML>";
		aml += "<Item type='" + "In_Timesheet_R" + j + "' action='delete' where=\"[source_id]='" + itmTimesheetAuditing.getID() + "'\">";
		aml += "</Item></AML>";
		Item itmTimesheet = inn.applyAML(aml);
	}
	
	//3.搜尋一天前,二天前,三天前各專案的工時紀錄表(In_TimeRecord),且生命週期狀態為In Review
	//專案工時審核表的專案欄位與工時紀錄表的專案名稱欄位必須相同
	aml = "<AML>";
	aml += "<Item type='In_TimeRecord' action='get'>";
	aml += "<state>In Review</state>";
	aml += "<in_project>" + itmTimesheetAuditing.getProperty("in_project","") + "</in_project>";
	aml += "</Item></AML>";
	Item itmTimeRecords = inn.applyAML(aml);
	for(int k=0;k<itmTimeRecords.getItemCount();k++){
		Item itmTimeRecord = itmTimeRecords.getItemByIndex(k);
		
		param = "<mode>diff</mode>";
		param += "<date>" + itmTimeRecord.getProperty("in_start_time","") + "</date>";
		param += "<date2>" + strNowDateTime + "</date2>";
		Item itmDelay = inn.applyMethod("PM_handleWorkDays",param);
		
		string strDelay = itmDelay.getProperty("r","");
		string strStartTime = "";
		
		int intDelay = Convert.ToInt32(strDelay);
		
		//4.將超過三天的專案工時審核表狀態推為Approved
		if(intDelay > 3){
		    
		    itmTimeRecord.setProperty("state","Approved");
		    itmTimeRecord = itmTimeRecord.apply("promoteItem");
		}else{
		    strStartTime = itmTimeRecord.getProperty("in_start_time","");
		    strStartTime = strStartTime.Split('T')[0];
		}
		
		//5.將工時紀錄表分別增加至各關聯:工時審核-今天,工時審核-昨天,工時審核-前天
		aml = "";
		
		if(strStartTime == strWorkDay[0]){
			aml = "<AML>";
			aml += "<Item type='In_Timesheet_R1' action='add'>";
		}
		
		if(strStartTime == strWorkDay[1]){
			aml = "<AML>";
			aml += "<Item type='In_Timesheet_R2' action='add'>";
		}
		
		if(strStartTime == strWorkDay[2]){
			aml = "<AML>";
			aml += "<Item type='In_Timesheet_R3' action='add'>";
		}
		
		if(aml != ""){
			aml += "<source_id>" + itmTimesheetAuditing.getID() + "</source_id>";
			aml += "<related_id>" + itmTimeRecord.getID() + "</related_id>";
			aml += "</Item></AML>";
			Item itmAdd = inn.applyAML(aml);
		}
	}
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Refresh_Timesheet_Auditing' and [Method].is_current='1'">
<config_id>AE34FAFE99454F098C1850020E36A20D</config_id>
<name>In_Refresh_Timesheet_Auditing</name>
<comments>產生每日的專案工時審核表(In_Timesheet_Auditing)</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
