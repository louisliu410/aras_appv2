/*
目的:將檔名上的特殊字元@ = & ' # % { } ~ [  ]一律置換成-符號
做法:
*/
string strMethodName = "In_RemoveFileReservedChar";
//System.Diagnostics.Debugger.Break();

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");

sql += "Update [innovator].[File] set filename = Replace(filename,'@','-'),keyed_name = Replace(filename,'@','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'=','-'),keyed_name = Replace(filename,'=','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'&','-'),keyed_name = Replace(filename,'&','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'''','-'),keyed_name = Replace(filename,'''','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'#','-'),keyed_name = Replace(filename,'#','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'%','-'),keyed_name = Replace(filename,'%','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'{','-'),keyed_name = Replace(filename,'{','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'}','-'),keyed_name = Replace(filename,'}','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'~','-'),keyed_name = Replace(filename,'~','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,'[','-'),keyed_name = Replace(filename,'[','-') where id='" + this.getID() + "';";
sql += "Update [innovator].[File] set filename = Replace(filename,']','-'),keyed_name = Replace(filename,']','-') where id='" + this.getID() + "';";

inn.applySQL(sql);
return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_RemoveFileReservedChar' and [Method].is_current='1'">
<config_id>C66688DB715E4CDF9364C030CD875796</config_id>
<name>In_RemoveFileReservedChar</name>
<comments>將檔名上的特殊字元@ = &amp; &apos; # % { } ~ [  ]一律置換成-符號</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
