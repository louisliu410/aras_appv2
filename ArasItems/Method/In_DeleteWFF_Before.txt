/*
目的:流程功能的物件刪除時,順便刪除 In_WorkflowForm
位置: onBeforeDelete
*/
//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();
string sql = "Select in_wff_id from [" + this.getType().Replace(" ","_") + "] where id='" + this.getID() + "'";
Item itmThisItem = inn.applySQL(sql);
if(itmThisItem.getProperty("in_wff_id","")!="")
{
	sql = "Update [In_WorkflowForm] set in_item_id='delete' where id='" + itmThisItem.getProperty("in_wff_id","") + "'";
	inn.applySQL(sql);
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_DeleteWFF_Before' and [Method].is_current='1'">
<config_id>1691FA9BCD104E4AB848B899809A7809</config_id>
<name>In_DeleteWFF_Before</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
