//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string aml = "";
string sql = "";
Item itmPermission = null;
string strPermissionName = this.getType() + "_" + this.getProperty("state","New").Replace(" ","");

aml = "<AML>";
aml += "<Item type='Permission' action='get'>";
aml += "<name>"+strPermissionName+"</name>";
aml += "</Item></AML>";
itmPermission = inn.applyAML(aml);

if(itmPermission.isError())
{
    strPermissionName = this.getType() + "_" + "New";
    
    aml = "<AML>";
    aml += "<Item type='Permission' action='get'>";
    aml += "<name>"+strPermissionName+"</name>";
    aml += "</Item></AML>";
    itmPermission = inn.applyAML(aml);
}

sql = "UPDATE " + this.getType().Replace(" ","_") + " SET permission_id = '" + itmPermission.getID() + "' WHERE ID = '" + this.getID() +"'";
inn.applySQL(sql);

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_afterCopy' and [Method].is_current='1'">
<config_id>31A4FD244B284C52BE11AE66A5AE007F</config_id>
<name>In_afterCopy</name>
<comments>permission_id必須等於目前狀態所對應的permission id,若找不到,則使用default permission</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
