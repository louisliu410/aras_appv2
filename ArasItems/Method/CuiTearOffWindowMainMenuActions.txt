	Item menuItems = null;
	//ItemType and Item actions
	string itemTypeId = this.getProperty("item_type_id");
	if (!string.IsNullOrEmpty(itemTypeId))
	{
		Item itemActions = this.newItem("Item Action", "get");
		itemActions.setProperty("source_id", itemTypeId);
		Item relActions = this.newItem("Action", "get");
		relActions.setAttribute("select", "name,label,can_execute,location");
		Item logicOr = relActions.newOR();
		logicOr.setProperty("type", "itemtype");
		logicOr.setProperty("type", "item");
		itemActions.setRelatedItem(relActions);
		itemActions = itemActions.apply();
		if (!itemActions.isError())
		{
			Item initHandler = GetMethodByConfigId("723C33B7F24B4523A6D1118AC993CDF9");
			Item actions = itemActions.getItemsByXPath(Item.XPathResultItem + "/related_id/Item");
			menuItems = HandleMenuButtons(actions, "action:{0}:{1}", itemTypeId, initHandler);
		}
	}

	return menuItems ?? this.getInnovator().newResult("");
}

private Item HandleMenuButtons(Item actions, string actionTemplate, string itemTypeId, Item initHandler)
{
	int sortOrder = 0;
	int actionsCount = actions.getItemCount();
	if (actionsCount > 0)
	{
		string initHandlerId = initHandler.getID();
		string initHandlerName = initHandler.getProperty("name");
		Item cuiItems = actions.newItem();
		for (var i = 0; i < actionsCount; i++)
		{
			Item currentAction = actions.getItemByIndex(i);
			Item menuButton = currentAction.newItem("CommandBarMenuButton");
			string actionId = currentAction.getID();
			string actionName = string.Format(CultureInfo.InvariantCulture, actionTemplate, actionId, itemTypeId);
			menuButton.setProperty("name", actionName);
			menuButton.setProperty("label", currentAction.getProperty("label") ?? currentAction.getProperty("name"));

			string canExecuteMethodName = currentAction.getPropertyAttribute("can_execute", "keyed_name");
			string location = currentAction.getProperty("location");
			menuButton.setProperty("additional_data", "{\"canExecuteMethodName\": \"" + canExecuteMethodName + "\", \"location\": \"" + location + "\"}");
			// Need to add 'can_execute' method to Convert CAD To PDF Action, but it cann't be modified in SP.
			// Convert CAD To PDF needs special init handler that allows to handle disabling/enbling of action depend on license
			// Special logic for Convert CAD To PDF Action should be removed in major release.
			if (actionId == "9F34709DE5284912A65D826624FBB9F7")
			{
				// '0DAB745E3E32405CA9A1DA9D37BB2968' - CadToPdfTearOffMenuInitHandler
				Item cadToPdfInitHandler = GetMethodByConfigId("0DAB745E3E32405CA9A1DA9D37BB2968");
				menuButton.setProperty("on_init_handler", cadToPdfInitHandler.getID());
				menuButton.setPropertyAttribute("on_init_handler", "keyed_name", cadToPdfInitHandler.getProperty("name"));
			}
			else
			{
				menuButton.setProperty("on_init_handler", initHandlerId);
				menuButton.setPropertyAttribute("on_init_handler", "keyed_name", initHandlerName);
			}
			menuButton.setPropertyAttribute("on_init_handler", "type", "Method");

			// '09129412D2ED4CB19C39EBFC0C6FDAAF' - com.aras.innovator.cui_default.twmm_actions_menu
			menuButton.setProperty("parent_menu", "09129412D2ED4CB19C39EBFC0C6FDAAF");
			menuButton.setProperty("action", "Add");
			sortOrder += 128;
			menuButton.setProperty("sort_order", sortOrder.ToString(CultureInfo.InvariantCulture));
			menuButton.setID(actionId);
			menuButton.removeAttribute("isNew");
			menuButton.removeAttribute("isTemp");
			cuiItems.appendItem(menuButton);
		}

		// newItem() is creates empty node. It's should be deleted
		cuiItems.removeItem(cuiItems.getItemByIndex(0));
		return cuiItems;
	}

	return null;
}

private Item GetMethodByConfigId(string configId)
{
	Item method = this.newItem("Method", "get");
	method.setAttribute("select", "id,name");
	method.setProperty("config_id", configId);
	return method.apply();
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CuiTearOffWindowMainMenuActions' and [Method].is_current='1'">
<config_id>F31525FEFDAB401FACABF8B927373D75</config_id>
<name>CuiTearOffWindowMainMenuActions</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
