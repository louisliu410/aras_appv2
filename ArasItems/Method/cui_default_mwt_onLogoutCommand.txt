var topWindow = aras.getMostTopWindowWithAras(window);
var menuFrame = topWindow.menu;
if (menuFrame && menuFrame.onLogoutCommand) {
	menuFrame.onLogoutCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onLogoutCommand' and [Method].is_current='1'">
<config_id>70839E07BC424F07ACD82C893C762A3D</config_id>
<name>cui_default_mwt_onLogoutCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
