if(top.aras.isDirtyEx(inDom) || top.aras.isNew(inDom))
{
    alert(this.getInnovator().applyMethod("In_Translate","<text>請先儲存本文件再執行</text>").getResult());
    return;
}

if(this.getLockStatus()!==0)
{
    alert(this.getInnovator().applyMethod("In_Translate","<text>請先解鎖本文件再執行</text>").getResult());
	return;
}

var inn = this.getInnovator();
var itmProject = this.apply("In_QuoteToProject_Action");
if(itmProject.isError())
	alert(itmProject.getErrorString());
else
	top.aras.uiShowItemEx(itmProject.node, "tab view");
		
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_QuoteToProject_C' and [Method].is_current='1'">
<config_id>C61FF25AC4EC446793D39E2F4121DB8E</config_id>
<name>In_QuoteToProject_C</name>
<comments>inn flow</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
