/*
    取得指定物件類型及id，並且fetch指定的relationshi取得指定物件類型及id，並且fetch指定的relationship。
    回傳一個ARAS標準物件。結構如下：
    <Item>
    <...>...<.../>
    <Relationship>       
        ..
    </Relationship>
    </Item>
    
    傳入參數：
        itemtype : 物件的類型
        itemid : 物件的id
        rels    :要fetch的relationship類型，csv字串
        mode :抓取資料的方式，
            選項：
                latest :抓取最新版本
                direct :抓取id所指定的版本
       
       //break 快取
*/
Innovator inn=this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
string strItemtype=this.getProperty("itemtype","no_data");
string strItemname=this.getProperty("itemname","no_data");
string strItemid=this.getProperty("itemid","no_data");
string strRelationCSV=this.getProperty("rels","no_data");
string strQueryMode=this.getProperty("mode","direct");

try{
    //防呆
    if(strItemtype=="no_data"||(strItemid=="no_data" && strItemname=="no_data")||strRelationCSV=="no_data"||strQueryMode=="no_data"){
        string errMsg="Missing Required Parameter itemtype:"+strItemtype+" itemid="+strItemid+"rels="+strRelationCSV+" mode= "+strQueryMode;
        return inn.newError(errMsg);
    }
    

    Item itmQ=inn.newItem(strItemtype,"get");
    if(strItemid!="no_data"){
    itmQ.setID(strItemid);    
    }else{
        if(strItemname!="no_data"){
            itmQ.setProperty("name",strItemname);
        }
        
    }
    
    itmQ=itmQ.apply();
    string[] toFetch=strRelationCSV.Split(new char[]{','});


    foreach(string fetchType in toFetch){
		string[] strListToFetch;
		Dictionary<string,Item> listValues=new Dictionary<string,Item>();

			Item itmFetchQ=inn.newItem("Method","In_Get_PropertyFetchList");
			itmFetchQ.setProperty("typeToFetch",fetchType);
			itmFetchQ.setProperty("datatype","list");
			strListToFetch=itmFetchQ.apply().getResult().Split(new Char[]{','});
			
		foreach(string listProp in strListToFetch){
			Item itmRelIt=inn.newItem("ItemType","get");
			itmRelIt.setProperty("name",fetchType);
			itmRelIt=itmRelIt.apply();
			Item valueQ=inn.newItem("Property","get");
			valueQ.setProperty("name",listProp);
			valueQ.setProperty("source_id",itmRelIt.getID());
			valueQ=valueQ.apply();
			
			Item itmList=inn.getItemById("List",valueQ.getProperty("data_source"));
			listValues.Add(listProp,itmList);
			
			
		}
		
		
		

		Item itmRelType=inn.newItem("RelationshipType","get");
		itmRelType.setProperty("name",fetchType);
		itmRelType.setProperty("source_id",itmQ.getAttribute("typeId"));				
		itmRelType=itmRelType.apply();
		itmRelType.fetchRelationships("Relationship View");
		Item itmRV=itmRelType.getRelationships("Relationship View");
			if(itmRV.getItemCount()>0){
				itmRelType.setProperty("source_id",itmQ.getID());
				itmRelType.setProperty("rel_itemtype",itmRelType.getProperty("name"));
				itmRelType.setProperty("source_itemtype",itmQ.getType());
				Item itmRel=itmRelType.apply("In_GetRel_N");
				int relCount=itmRel.getItemCount();
				for(int y=0;y<relCount;y++){
					itmQ.addRelationship(itmRel.getItemByIndex(y));
				}
			}else{
				itmQ.fetchRelationships(fetchType);				
				
			}
			
			Item itmLabelMatcher=inn.newItem("Method","In_Find_MatchedLabel");
			
				
		Item itmRels=itmQ.getRelationships(fetchType);
        int intRelItemCount=itmRels.getItemCount();
		/*
			//取得relationship 的related_item的fetchlist      
		
		*/
			
			string[] staRelFetches=new string[]{};
    		Dictionary<string,Item> relatedLists=new Dictionary<string,Item>();
            string strRelatedType="";
			if(intRelItemCount>0 ){
			    strRelatedType=itmRels.getItemByIndex(0).getPropertyAttribute("related_id","type","no_data");
			    if(strRelatedType!="no_data"){
			        	    		

				Item itmRelFetchQ=inn.newItem("Method","In_Get_PropertyFetchList");
				itmRelFetchQ.setProperty("typeToFetch",strRelatedType);
				itmRelFetchQ.setProperty("datatype","list");				
				staRelFetches=itmRelFetchQ.apply().getResult().Split(new char[]{','});
				
				
				foreach(string strFetches in  staRelFetches){
				    if(strFetches==""){continue;}
					Item itmRelIt=inn.newItem("ItemType","get");
					itmRelIt.setProperty("name",strRelatedType);
					itmRelIt=itmRelIt.apply();
					Item valueQ=inn.newItem("Property","get");
					valueQ.setProperty("name",strFetches);
					valueQ.setProperty("source_id",itmRelIt.getID());
					valueQ=valueQ.apply();
					
					Item itmList=inn.getItemById("List",valueQ.getProperty("data_source"));
					relatedLists.Add(strFetches,itmList);

				    }
			        
			    }
		    	

			
				
				
				
				
			}
			
			
		
		
		
		
		/*
			//取得relationship 的related_item的fetchlist
		*/
		
        for(int x=0;x<intRelItemCount;x++){
            Item itmL=itmRels.getItemByIndex(x);
            Item itmRelOfRel=itmL.getRelatedItem();
            string  strRelatedName="";
			foreach(string listProp in strListToFetch){
				itmLabelMatcher.setProperty("property_value",itmL.getProperty(listProp,""));
				itmLabelMatcher.setPropertyItem("collection",listValues[listProp]);
				itmL.setProperty(listProp+".label",itmLabelMatcher.apply().getResult());
			}
			
			if(itmRelOfRel!=null){
				foreach(string relFetchProp in staRelFetches ){
				    if(relFetchProp==""){continue;}
					itmLabelMatcher.setProperty("property_value",itmRelOfRel.getProperty(relFetchProp,""));
					itmLabelMatcher.setPropertyItem("collection",relatedLists[relFetchProp]);
					itmRelOfRel.setProperty(relFetchProp+".label",itmLabelMatcher.apply().getResult());
					itmRelOfRel.setProperty(relFetchProp+".referredListName",relatedLists[relFetchProp].getProperty("name",""));
					itmRelOfRel.setProperty(relFetchProp+".referredListId",relatedLists[relFetchProp].getID());
					
				}
				
				
				
				
			}

            if(itmRelOfRel!=null){
                strRelatedName=itmRelOfRel.getProperty("keyed_name");
            }
            itmL.setProperty("source_id.label",strRelatedName);
            itmL.setProperty("related_id.label",strRelatedName);
            itmL.setProperty("related_id.keyed_name",strRelatedName);
            
         
	
	

        }
		Item itmNrels = itmQ.getRelationships(fetchType);
				int intNcount=itmNrels.getItemCount();
				for(int us=0;us<intNcount;us++){
				    Item itmSrel=itmNrels.getItemByIndex(us);
				    Item itmChild=itmSrel.getRelatedItem();
				    if(itmChild==null){continue;}
				    System.Xml.XmlNodeList cProps=itmChild.node.ChildNodes;
				    foreach(System.Xml.XmlNode node in cProps){
				        System.Xml.XmlAttributeCollection attrs= node.Attributes;
				        if(attrs!=null){
				            foreach(XmlAttribute attr in attrs){
				                if(attr.Name.Contains(":")){
				                    continue;
				                }
				                itmSrel.setProperty("c_"+node.Name+"."+attr.Name,attr.Value);
				                
				            }
				        }
				        itmSrel.setProperty("c_"+node.Name,node.InnerText);
				    }
				}

    }
    
    itmQ = itmQ.apply("In_AppendExtraProperties");

    return itmQ;
    
    
    
    
}catch(Exception ex){
        string strError = ex.Message + "\n";
	string strErrorDetail="";
	string strParameter="\n itemtype="+strItemtype;
	_InnH.AddLog(strErrorDetail+strParameter,"Error");
	strError = strError.Replace("<br/>","");

    throw new Exception(_InnH.Translate(strError));
	//throw new Exception(_InnH.Translate(strError+strParameter));

    
}


#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_Get_ItemAndRelationship' and [Method].is_current='1'">
<config_id>0C930D0642784BD4AFDB95BC0E71D9EC</config_id>
<name>In_Get_ItemAndRelationship</name>
<comments>用來取得指定類別物件的本體及特定類型的Relationship。主要由c.aspx呼叫。</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
