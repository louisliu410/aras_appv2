/*
目的:客戶費用請款單可以挑單身
做法:
1.判斷合約的欄位是否有值
2.取得關係類型
3.關係類型有值就跳出提醒
位置:
*/

//System.Diagnostics.Debugger.Break();
Innovator inn = this.getInnovator();

string aml = "";

Item itmVendorPaymentDetail = null;
Item itmVendorPaymentdetail2 = null;

aml = "<AML>";
aml += "<Item type='In_Invoice_detail_2' action='get'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "</Item></AML>";
Item itmVendorPaymentdetail2s = inn.applyAML(aml);

for(int i=0;i<itmVendorPaymentdetail2s.getItemCount();i++){
    itmVendorPaymentdetail2 = itmVendorPaymentdetail2s.getItemByIndex(i);
}

if(itmVendorPaymentdetail2 != null && this.getProperty("in_contract","") ==""){
    return inn.newError("無合約時不可挑選合約請款規劃");
}
if(itmVendorPaymentdetail2 == null && this.getProperty("in_contract","") !=""){
    return inn.newError("有合約時必須挑選合約請款規劃");
}

aml = "<AML>";
aml += "<Item type='In_Invoice_Details' action='get'>";
aml += "<source_id>"+this.getID()+"</source_id>";
aml += "</Item></AML>";
Item itmVendorPaymentDetails = inn.applyAML(aml);

for(int i=0;i<itmVendorPaymentDetails.getItemCount();i++){
    itmVendorPaymentDetail = itmVendorPaymentDetails.getItemByIndex(i);
}

if(itmVendorPaymentDetail == null && this.getProperty("in_contract","") ==""){
    return inn.newError("無合約時必須挑選銷貨項目");
}
if(itmVendorPaymentDetail != null && this.getProperty("in_contract","") !=""){
    return inn.newError("有合約時不可挑選銷貨項目");
}

return this;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_ValidateInvoicePayment' and [Method].is_current='1'">
<config_id>A7C69FDE449C4B20A27A70AAFA4BC5F1</config_id>
<name>In_ValidateInvoicePayment</name>
<comments>客戶費用請款單可以挑單身</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
