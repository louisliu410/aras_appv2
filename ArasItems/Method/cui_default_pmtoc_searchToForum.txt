var topWindow = aras.getMostTopWindowWithAras(window);
var treeFrame = topWindow.tree;
if (treeFrame && treeFrame.SavedSearchInToc) {
	var ssInToc = new treeFrame.SavedSearchInToc(inArgs.rowId);
	ssInToc.addSavedSearchToForum();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_pmtoc_searchToForum' and [Method].is_current='1'">
<config_id>83D81CD014CC48F590039633D9D9A1D1</config_id>
<name>cui_default_pmtoc_searchToForum</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
