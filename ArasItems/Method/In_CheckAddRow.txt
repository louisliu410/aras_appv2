var aml = "";
var inn = this.getInnovator();
var strRelName = this.toString();
var SessionIdentitites = top.aras.getIdentityList();

strRelName = strRelName.replace("\"","");
strRelName = strRelName.replace("<Item type=","");
strRelName = strRelName.replace("/>","");
strRelName = strRelName.replace("\"","");

aml  = "<AML>";
aml += "<Item type='RelationshipType' action='get'>";
aml += "<name>"+strRelName+"</name>";
aml += "</Item></AML>";
var itmRel = inn.applyAML(aml);

strRelName = itmRel.getProperty("label","");

aml  = "<AML>";
aml += "<Item type='Identity' action='get'>";
aml += "<name>Aras PLM</name>";
aml += "</Item></AML>";
var itmIdentity = inn.applyAML(aml);

if(SessionIdentitites.indexOf(itmIdentity.getID())<0)
{
    
    alert(inn.applyMethod("In_Translate","<text>目前沒有權限操作"+strRelName + "</text>").getResult());
    return this;
}
else
{
    
    alert(inn.applyMethod("In_Translate","<text>目前無法手動新增"+strRelName + "</text>").getResult());
    return this;
}
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_CheckAddRow' and [Method].is_current='1'">
<config_id>E5F7FCAFDBFA48E28D7E0B2532933646</config_id>
<name>In_CheckAddRow</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
