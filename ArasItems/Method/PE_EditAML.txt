// Called from the Part AML Editor form. Allows limited editing of a released Part,
//  allowing only allows changes to the Part AML relationships.

// Grant 'Aras PLM' permissions
Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Aras PLM");
Boolean PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator innovator = this.getInnovator();

Item q = innovator.applyAML(String.Format(CultureInfo.InvariantCulture,
	"<AML>" +
	"	<Item type='Part' action='get' id='{0}' select='state'>" +
	"		<state>{1}</state>" +
	"	</Item>" +
	"</AML>",
	this.getAttribute("id"),
	"Manual Change"));

if (q.isError() || q.isEmpty())
	return innovator.newError(q.getErrorString());

//+++++ Validate that only Part AML relationships are being edited
if (this.getItemsByXPath("Relationships/Item[@action and @type != 'Part AML']").getItemCount() > 0)
	return innovator.newError("Relationships with type != 'Part AML' are not allowed to be modified in this context.");
//----- Validate that only Part AML relationships are being edited

this.setAction("edit");
this.setAttribute("version", "0");
this.setAttribute("doGetItem", "0");
q = this.apply();
//----- Apply the change

if (PermissionWasSet)
	Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return q;
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='PE_EditAML' and [Method].is_current='1'">
<config_id>36C65BE0928B4C4294618166282E46D1</config_id>
<name>PE_EditAML</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
