//Copied from CustomActionHelper.js
var isEnabled = aras.commonProperties.isConvertCADToPDFEnabled;
if (typeof(isEnabled) === 'undefined') {
	try {
		//IOM controls are hosted in the main window and are shared between other windows.
		//So, if tearoff window calls IomInnovator.ConsumeLicense inside of try catch block
		//and exception is occured then it appears in the main window as script error and only
		//after that will be handled by "catch" block of tearoff window.
		//To avoid described behaviour 'aras.ConsumeLicense' is used from main window.
		var mainWindow = aras.getMainWindow();
		var license = mainWindow.aras.ConsumeLicense('Aras.CADConverter2');
		isEnabled = !!license.result;
	} catch (exception) {
		isEnabled = false;
	}
	aras.commonProperties.isConvertCADToPDFEnabled = isEnabled;
}

return {'cui_disabled': !isEnabled};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='CadToPdfTearOffMenuInitHandler' and [Method].is_current='1'">
<config_id>0DAB745E3E32405CA9A1DA9D37BB2968</config_id>
<name>CadToPdfTearOffMenuInitHandler</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
