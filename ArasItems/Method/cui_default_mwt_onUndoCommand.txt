var topWindow = aras.getMostTopWindowWithAras(window);
var workerFrame = topWindow.work;
if (workerFrame && workerFrame.onUndoCommand) {
	workerFrame.onUndoCommand();
}

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_default_mwt_onUndoCommand' and [Method].is_current='1'">
<config_id>E58ADCAA92C9498A957141CBBE8867C1</config_id>
<name>cui_default_mwt_onUndoCommand</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
