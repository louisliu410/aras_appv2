if (inArgs.isReinit) {
	if (Object.getOwnPropertyNames(inArgs.eventState).length === 0) {
		var states = aras.evalMethod('cui_reinit_calculate_states', '', {eventType: inArgs.eventType});
		var keys = Object.keys(states);
		for (var i = 0; i < keys.length; i++) {
			inArgs.eventState[keys[i]] = states[keys[i]];
		}
	}

	var topWindow = aras.getMostTopWindowWithAras(window);
	var workerFrame = topWindow.work;
	if (workerFrame && workerFrame.currQryItem) {
		var queryItem = workerFrame.currQryItem.getResult();
		var grid = workerFrame.grid;
		if (grid) {
			var itemId = grid['getSelectedId_Experimental']();
			var itemNd = queryItem.selectSingleNode('Item[@id="' + itemId + '"]') || aras.getFromCache(itemId);
			var itemTypeName = aras.getItemTypeName(aras.getItemProperty(itemNd, 'itemtype'));
			return {'cui_disabled': !(inArgs.eventState.isLock && !isFunctionDisabled(itemTypeName, 'Lock'))};
		}
	}
}
return {};

#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='cui_reinit_mwt_inbasket_lock' and [Method].is_current='1'">
<config_id>D6125E8A1FB147A18C1BBEB44D4EBF6B</config_id>
<name>cui_reinit_mwt_inbasket_lock</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>JavaScript</method_type>
</Item>
</AML>
