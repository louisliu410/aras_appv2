/*
目的:提供APP本次登入的基本資訊
做法:
1.提供目前登入者所屬 identity 的 name list
2.提供目前登入者的 user identity 資訊,以便作為 client cache default 之用
*/
string strMethodName = "In_GetSessionData";
//System.Diagnostics.Debugger.Break();
//Aras.Server.Security.Identity plmIdentity = Aras.Server.Security.Identity.GetByName("Super User");
//    bool PermissionWasSet = Aras.Server.Security.Permissions.GrantIdentity(plmIdentity);

Innovator inn = this.getInnovator();
Innosoft.InnovatorHelper _InnH = new Innosoft.InnovatorHelper(inn);
	string UserInfo = this.getProperty("userinfo","");
Innosoft.app _InnoApp = new Innosoft.app(inn,UserInfo);
string aml = "";
string sql = "";

_InnH.AddLog(strMethodName,"MethodSteps");
Item itmR = this;

try
{
	Item identity=inn.newItem("Identity","get");
	identity.setAttribute("select","name");
	identity.setProperty("id",Aras.Server.Security.Permissions.Current.IdentitiesList);
	identity.setPropertyCondition("id","in");
	identity=identity.apply();
	List<string> names=new List<string>();
	for(int i=0;i<identity.getItemCount();i++){
		names.Add(identity.getItemByIndex(i).getProperty("name"));
	}
	itmR=this.newItem();
	itmR.setProperty("identity_list",string.Join(",",names.ToArray()));



	string strLoginName = _InnoApp.GetUserInfo(UserInfo, "loginid");
	Item itmIdentity = _InnH.GetIdentityByUserLoginName(strLoginName);
	Item itmUser = _InnH.GetUserByAliasIdentityId(itmIdentity.getID());



	string strSession_Data = "";
	strSession_Data = "<session_data>";
	strSession_Data += "<user_id>" + itmUser.getID() + "</user_id>";
	strSession_Data += "<user_keyed_name>" + itmUser.getProperty("keyed_name") + "</user_keyed_name>";
	strSession_Data += "<identity_id>" + itmIdentity.getID() + "</identity_id>";
	strSession_Data += "<identity_keyed_name>" + itmIdentity.getProperty("keyed_name") + "</identity_keyed_name>";
	strSession_Data += "</session_data>";

	//itmR.setProperty("session_data",System.Web.HttpUtility.HtmlEncode(strSession_Data));
	itmR.setProperty("session_data",strSession_Data);




}
catch(Exception ex)
{	
	//if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);


	string strFullMethodInfo = "[" + this.getType() + "]." + this.getID() + ":" + strMethodName;


	string strError = ex.Message + "\n";

	if(aml!="")
    	strError += "無法執行AML:" + aml  + "\n";

	if(sql!="")
		strError += "無法執行SQL:" + sql  + "\n";

	string strErrorDetail="";
	strErrorDetail = strFullMethodInfo + "\n" + strError + "\n" + ex.ToString() + "\n" + ex.StackTrace.ToString();
	_InnH.AddLog(strErrorDetail,"Error");
	strError = strError.Replace("\n","</br>");
	throw new Exception(_InnH.Translate(strError));
}
//if (PermissionWasSet) Aras.Server.Security.Permissions.RevokeIdentity(plmIdentity);

return this.getInnovator().newResult(itmR.dom.InnerXml);
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='In_GetSessionData' and [Method].is_current='1'">
<config_id>CEFAD633D415466E9CA74E06846CEAE7</config_id>
<name>In_GetSessionData</name>
<comments>提供APP本次登入的基本資訊</comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
