			return Execute(this, tmpItem => tmpItem.apply());
		}

		public static Item Execute(Item self, Func<Item, Item> applyFunc)
		{
			if (self == null || applyFunc == null)
			{
				return null;
			}
			string instanceData = self.getProperty("instance_data").Replace("'", "''"); //do a replace to reduce SQL injections chance
			string generationSql = String.Format(CultureInfo.InvariantCulture,
				"SELECT MAX(generation) FROM [{0}] maxreleasedinset WHERE maxreleasedinset.config_id=[{0}].config_id AND " +
				"maxreleasedinset.is_released='1' AND maxreleasedinset.is_current='0'", instanceData);

			string configId = self.getProperty("config_id");
			string itemTypeName = self.getType();
			Innovator inn = self.getInnovator();
			Item query = inn.newItem(itemTypeName, "get");
			query.setAttribute("type", itemTypeName);
			query.setProperty("config_id", configId);
			query.setProperty("generation", generationSql);
			query.setPropertyAttribute("generation", "condition", "in");
			Item result = applyFunc(query);
			return result;
			
#innosoft#
<AML>
<Item type='Method' where="[Method].[Name]='GetReleasedMaxGenerationRelGrid' and [Method].is_current='1'">
<config_id>0369AABC5F5F48A09BC0B6255F23FDD6</config_id>
<name>GetReleasedMaxGenerationRelGrid</name>
<comments></comments>
<in_ver></in_ver>
<in_ver_note></in_ver_note>
<method_type>C#</method_type>
</Item>
</AML>
